import random


def create_jack_dictionary(words_list):
    with open(words_list) as f:
        lines = f.read().splitlines()
    f.close()
    len_list = len(lines)
    out_file = open('Dictionary.jack', 'w')
    out_file.write('class Dictionary {\n')
    out_file.write('\tfield int curWord;\n')
    out_file.write('\tconstructor Dictionary new() {\n')
    out_file.write('\t\tlet curWord = -1;\n')
    out_file.write('\t\treturn this;\n')
    out_file.write('\t}\n')
    out_file.write('\tmethod void randInt() { \n')
    out_file.write('\t\t let curWord = curWord + 1;  \n')
    out_file.write('\t\t return;  \n')
    out_file.write('\t}\n')
    out_file.write('\tmethod String getRandomWord() {\n')
    out_file.write('\t\tdo randInt() ;\n')
    random.shuffle(lines)
    for idx, word in enumerate(lines):
        out_file.write('\t\tif (curWord={})'.format(idx) + '{')
        out_file.write('return "{}";'.format(word.upper()))
        out_file.write('}\n')
    out_file.write('\treturn "DEFAULT";\n\t}\n')
    out_file.write('}\n')
    out_file.close()

if __name__ == '__main__':
    create_jack_dictionary("wordList.txt")


