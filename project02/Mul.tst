load Mul.hdl,
output-file Mul.out,
compare-to Mul.cmp,
output-list a%D1.16.1 b%D1.16.1 out%D1.16.1;

set a %D0,
set b %D1,
eval,
output;

set a %D1,
set b %D0,
eval,
output;

set a %D13,
set b %D56,
eval,
output;

set a %D-1,
set b %D0,
eval,
output;

set a %D0,
set b %D-1,
eval,
output;

set a %D-10,
set b %D6,
eval,
output;

set a %D15,
set b %D-3,
eval,
output;

set a %D-17,
set b %D-25,
eval,
output;

set a %B0100000000000000,
set b %B0100000000000000,
eval,
output;

set a %B0100000000000000,
set b %B0000000000000010,
eval,
output;

set a %B0000000000000000,
set b %B0000000000000000,
eval,
output;

set a %B1000000000000000,
set b %B1000000000000000,
eval,
output;

set a %B1000000000000000,
set b %B0100000000000000,
eval,
output;

set a %B1000000000000000,
set b %B0010000000000000,
eval,
output;

set a %B1000000000000000,
set b %B0001000000000000,
eval,
output;

set a %B1000000000000000,
set b %B0000100000000000,
eval,
output;

set a %B1000000000000000,
set b %B0000010000000000,
eval,
output;

set a %B1000000000000000,
set b %B0000001000000000,
eval,
output;

set a %B1000000000000000,
set b %B0000000100000000,
eval,
output;

set a %B1000000000000000,
set b %B0000000010000000,
eval,
output;

set a %B1000000000000000,
set b %B0000000001000000,
eval,
output;

set a %B1000000000000000,
set b %B0000000000100000,
eval,
output;

set a %B1000000000000000,
set b %B0000000000010000,
eval,
output;

set a %B1000000000000000,
set b %B0000000000001000,
eval,
output;

set a %B1000000000000000,
set b %B0000000000000100,
eval,
output;

set a %B1000000000000000,
set b %B0000000000000010,
eval,
output;

set a %B1000000000000000,
set b %B0000000000000001,
eval,
output;

set a %B1000000000000000,
set b %B0000000000000000,
eval,
output;

set a %B0100000000000000,
set b %B1000000000000000,
eval,
output;

set a %B0100000000000000,
set b %B1000000000000000,
eval,
output;

set a %B0010000000000000,
set b %B1000000000000000,
eval,
output;

set a %B0001000000000000,
set b %B1000000000000000,
eval,
output;

set a %B0000100000000000,
set b %B1000000000000000,
eval,
output;

set a %B0000010000000000,
set b %B1000000000000000,
eval,
output;

set a %B0000001000000000,
set b %B1000000000000000,
eval,
output;

set a %B0000000100000000,
set b %B1000000000000000,
eval,
output;

set a %B0000000010000000,
set b %B1000000000000000,
eval,
output;

set a %B0000000001000000,
set b %B1000000000000000,
eval,
output;

set a %B0000000000100000,
set b %B1000000000000000,
eval,
output;

set a %B0000000000010000,
set b %B1000000000000000,
eval,
output;

set a %B0000000000001000,
set b %B1000000000000000,
eval,
output;

set a %B0000000000000100,
set b %B1000000000000000,
eval,
output;

set a %B0000000000000010,
set b %B1000000000000000,
eval,
output;

set a %B0000000000000001,
set b %B1000000000000000,
eval,
output;

set a %D-1,
set b %D-1,
eval,
output;