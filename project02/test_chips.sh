#!/bin/bash
echo Running test2.sh
./test2.sh
echo
FILE_LIST='ALU  Add16  FullAdder  HalfAdder  Inc16 ShiftLeft ShiftRight Mul'
for CHIP in $FILE_LIST; do
  if [ ! -r $CHIP.hdl ]; then
    echo Chip $CHIP was not found
  else
	echo Testing $CHIP.hdl Chip on Hardware Emulator:
	 HardwareSimulator.bat $CHIP.tst
	echo 
  fi
done
