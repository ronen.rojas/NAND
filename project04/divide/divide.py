def divide(nom, den):
    res, counter = 0, 1
    while nom >= den:
        den <<= 1
        counter <<= 1
    while counter > 1:
        den >>= 1
        counter >>= 1
        if nom >= den:
            nom -= den
            res += counter
    return res


if __name__ == "__main__":
    a_lst = [(12, 2), (1, 1), (1, 3), (15, 3), (53, 2), (43, 3), (43, 43),
             (43, 5), (5349, 5),  (5349, 200), (200, 8574), (587, 67),
             (123, 7)]
    for a in a_lst:
            print('| ' + str(a[0]) + '| ' + str(a[1]) + '| ' +
                  str(a[0]//a[1]))

    for a in a_lst:
        print('| ' + str(a[0]) + '| ' + str(a[1]) + '| ' +
              str(a[0] * a[1]))
