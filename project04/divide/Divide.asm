// Divides R13 by R14 and stores the result in R15.
// (R13, R14, R15 refer to RAM[13], RAM[14], and RAM[15], respectively.)
//  Preforming long binary division
	@R13       //  Getting the numerator
	D=M
	@numerator
	M=D
	@R14       //  Getting the denominator
	D=M
	@denominator
	M=D
	@R15      // Nullifying R15 
	M=0
	@numerator    // Division has finished numerator is zero
	D=M
	@END
	D;JEQ
	@powerTwo // starting the powers of two counter
	M=1;
(WHILELOOP1)
	@numerator
	D=M
	@denominator 
	D=D-M
	@WHILELOOP2 // if numerator - denominator < 0  finished first while loop
	D;JLT
	@denominator 
	M=M<<
	@powerTwo
	M=M<<
	@WHILELOOP1
	0;JMP
(WHILELOOP2)
	@powerTwo // if powerTwo -1 <= 0 finished
	D=M-1
	@END
	D;JLE
	@powerTwo
	M=M>>
	@denominator 
	MD=M>>
	@numerator
	D=D-M
	@WHILELOOP2 // if denominator - numerator  > 0  dont add the result 
	D;JGT
	@denominator 
	D=M
	@numerator
	M=M-D
	@powerTwo
	D=M
	@R15
	M=M+D
	@WHILELOOP2
	0;JMP
(END)
