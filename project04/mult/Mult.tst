// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/mult/Mult.tst

load Mult.asm,
output-file Mult.out,
compare-to Mult.cmp,
output-list RAM[0]%D2.6.2 RAM[1]%D2.6.2 RAM[2]%D2.6.2;

set RAM[0] 0,   // Set test arguments
set RAM[1] 0,
set RAM[2] -1;  // Test that program initialized product to 0
repeat 20 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 1,   // Set test arguments
set RAM[1] 0,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 50 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 0,   // Set test arguments
set RAM[1] 2,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 80 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 3,   // Set test arguments
set RAM[1] 1,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 120 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 2,   // Set test arguments
set RAM[1] 4,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 150 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 6,   // Set test arguments
set RAM[1] 7,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 210 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 12,   // Set test arguments
set RAM[1] 2,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 500 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 1,   // Set test arguments
set RAM[1] 1,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 500 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 1,   // Set test arguments
set RAM[1] 3,
set RAM[2] -1;  // Ensure that program initialized product to 0
repeat 500 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 15,   // Set test arguments
set RAM[1] 3,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 53,   // Set test arguments
set RAM[1]  2,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 43,   // Set test arguments
set RAM[1]  3,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 43,   // Set test arguments
set RAM[1] 43,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;


set PC 0,
set RAM[0] 43,   // Set test arguments
set RAM[1]  5,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;


set PC 0,
set RAM[0] 5349,   // Set test arguments
set RAM[1]  5,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 123,   // Set test arguments
set RAM[1]  7,
set RAM[2] -1;  // Ensure that program initialized product to 0

repeat 1000 {
  ticktock;
}
output;



