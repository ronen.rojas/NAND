// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/mult/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

	@R2      // Nullifying R2 
	M=0
	@R1     // Getting the valuve of R1 
	D=M
	@counter
	M=D		// Putting the value of R1 in counter 
(LOOP)
	@counter    // Getting the current counter 
	D=M
	@END
	D;JEQ   // If the counter is zero the multiplication is over
	@R0     
	D=M
	@R2
	M=D+M
	@counter
	M=M-1 // Decreasing the counter by 1
	@LOOP
	0;JMP
(END)