#!/bin/bash
echo Running test4.sh
./test4.sh

echo ===================================================
echo Done running test4.sh
echo ===================================================
echo
Assembler.bat mult/Mult.asm 
echo
echo Testing mult/Mult.asm 
CPUEmulator.bat mult/mult.tst

echo
Assembler.bat fill/Fill.asm 
echo
echo Testing fill/Fill.asm 
CPUEmulator.bat fill/FillAutomatic.tst

echo
Assembler.bat sort/Sort.asm 
echo
echo Testing sort/Sort.asm with sort/Sort.tst
CPUEmulator.bat sort/Sort.tst
echo
echo Testing sort/Sort.asm with sort/Sort2.tst
CPUEmulator.bat sort/Sort2.tst
echo

echo Testing sort/Sort.asm with sort/Sort3.tst
CPUEmulator.bat sort/Sort3.tst
echo

echo There are no test for divide/Divide.asm! 
echo You need to test divide/Divide.asm in a different CPUEmulator 