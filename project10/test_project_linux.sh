#!/bin/bash

echo Tokenizing Square/
python3 jackanalyzer.py token_only diff_dir Square/
echo Comparing MainT.xml
diff -w Square/MainT.xml Square/output/MainT.xml
echo Comparing SquareT.xml
diff -w Square/SquareT.xml Square/output/SquareT.xml
echo Comparing SquareGameT.xml
diff -w Square/SquareGameT.xml Square/output/SquareGameT.xml
echo

echo Analyzing Square/
python3 jackanalyzer.py no_token diff_dir Square/
echo Comparing Main.xml
diff -w Square/Main.xml Square/output/Main.xml
echo Comparing Square.xml
diff -w Square/Square.xml Square/output/Square.xml
echo Comparing SquareGame.xml
diff -w Square/SquareGame.xml Square/output/SquareGame.xml
echo 
echo

echo Tokenizing ExpressionLessSquare/
python3 jackanalyzer.py token_only diff_dir ExpressionLessSquare/
echo Comparing MainT.xml
diff -w ExpressionLessSquare/MainT.xml ExpressionLessSquare/output/MainT.xml
echo Comparing SquareT.xml
diff -w ExpressionLessSquare/SquareT.xml ExpressionLessSquare/output/SquareT.xml
echo Comparing SquareGameT.xml
diff -w ExpressionLessSquare/SquareGameT.xml ExpressionLessSquare/output/SquareGameT.xml
echo

echo Analyzing ExpressionLessSquare/
python3 jackanalyzer.py no_token diff_dir ExpressionLessSquare/
echo Comparing Main.xml
diff -w ExpressionLessSquare/Main.xml ExpressionLessSquare/output/Main.xml
echo Comparing Square.xml
diff -w ExpressionLessSquare/Square.xml ExpressionLessSquare/output/Square.xml
echo Comparing SquareGame.xml
diff -w ExpressionLessSquare/SquareGame.xml ExpressionLessSquare/output/SquareGame.xml
echo
echo

echo Tokenizing ArrayTest/
python3 jackanalyzer.py token_only diff_dir ArrayTest/
echo Comparing MainT.xml
diff -w ArrayTest/MainT.xml ArrayTest/output/MainT.xml
echo

echo Analyzing ArrayTest/
python3 jackanalyzer.py no_token diff_dir ArrayTest/
echo Comparing Main.xml
diff -w ArrayTest/Main.xml ArrayTest/output/Main.xml
echo
echo


echo Tokenizing tabIssue/
python3 jackanalyzer.py token_only diff_dir tabIssue/
echo No tokenizer to compare!!
echo

echo Analyzing tabIssue/
python3 jackanalyzer.py no_token diff_dir tabIssue/
echo Comparing tabIssue.xml
diff -w tabIssue/tabIssue.xml tabIssue/output/tabIssue.xml
echo

echo Analyzing 1/
python3 jackanalyzer.py no_token diff_dir 1/
python3 tests.py 1/
echo

echo Analyzing 3/
python3 jackanalyzer.py no_token diff_dir 3/
python3 tests.py 3/
echo

echo Analyzing 5/
python3 jackanalyzer.py no_token diff_dir 5/
python3 tests.py 5/
echo

# Average
echo Analyzing Average\\
python3 jackanalyzer.py no_token diff_dir Average/
echo Comparing directory Average
python3 tests.py Average/
echo
echo
# ComplexArrays
echo Analyzing ComplexArrays\\
python3 jackanalyzer.py no_token diff_dir ComplexArrays/
echo Comparing directory ComplexArrays
python3 tests.py ComplexArrays/
echo
echo
# ConvertToBin
echo Analyzing ConvertToBin\\
python3 jackanalyzer.py no_token diff_dir ConvertToBin/
echo Comparing directory ConvertToBin
python3 tests.py ConvertToBin/
echo
echo
# GASboing
echo Analyzing GASboing\\
python3 jackanalyzer.py no_token diff_dir GASboing/
echo Comparing directory GASboing
python3 tests.py GASboing/
echo
echo
# JackScheme-master
echo Analyzing JackScheme-master\\
python3 jackanalyzer.py no_token diff_dir JackScheme-master/
echo Comparing directory JackScheme-master
python3 tests.py JackScheme-master/
echo
echo
# Pong
echo Analyzing Pong\\
python3 jackanalyzer.py no_token diff_dir Pong/
echo Comparing directory Pong
python3 tests.py Pong/
echo
echo
# Seven
echo Analyzing Seven\\
python3 jackanalyzer.py no_token diff_dir Seven/
echo Comparing directory Seven
python3 tests.py Seven/
echo
echo
# sokoban
echo Analyzing sokoban\\
python3 jackanalyzer.py no_token diff_dir sokoban/
echo Comparing directory sokoban
python3 tests.py sokoban/
echo
echo
# tictactoe-jack-master
echo Analyzing tictactoe-jack-master\\
python3 jackanalyzer.py no_token diff_dir tictactoe-jack-master/
echo Comparing directory tictactoe-jack-master
python3 tests.py tictactoe-jack-master/
echo
echo
# Square11
echo Analyzing Square11\\
python3 jackanalyzer.py no_token diff_dir Square11/
echo Comparing directory Square11
python3 tests.py Square11/
echo
echo

