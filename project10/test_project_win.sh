#!/bin/bash

# Square
echo Tokenizing Square\\
python.exe jackanalyzer.py token_only diff_dir Square/
echo Comparing MainT.xml
TextComparer.bat Square\\MainT.xml Square\\output\\MainT.xml
echo Comparing SquareT.xml
TextComparer.bat Square\\SquareT.xml Square\\output\\SquareT.xml
echo Comparing SquareGameT.xml
TextComparer.bat Square\\SquareGameT.xml Square\\output\\SquareGameT.xml
echo

echo Analyzing Square\\
python.exe jackanalyzer.py no_token diff_dir Square/
echo Comparing Main.xml
TextComparer.bat Square\\Main.xml Square\\output\\Main.xml
echo Comparing Square.xml
TextComparer.bat Square\\Square.xml Square\\output\\Square.xml
echo Comparing SquareGame.xml
TextComparer.bat Square\\SquareGame.xml Square\\output\\SquareGame.xml
echo 
echo

# Square ExpressionLessSquare
echo Tokenizing ExpressionLessSquare\\
python.exe jackanalyzer.py token_only diff_dir ExpressionLessSquare/
echo Comparing MainT.xml
TextComparer.bat ExpressionLessSquare\\MainT.xml ExpressionLessSquare\\output\\MainT.xml
echo Comparing SquareT.xml
TextComparer.bat ExpressionLessSquare\\SquareT.xml ExpressionLessSquare\\output\\SquareT.xml
echo Comparing SquareGameT.xml
TextComparer.bat ExpressionLessSquare\\SquareGameT.xml ExpressionLessSquare\\output\\SquareGameT.xml
echo

echo Analyzing ExpressionLessSquare\\
python.exe jackanalyzer.py no_token diff_dir ExpressionLessSquare/
echo Comparing Main.xml
TextComparer.bat ExpressionLessSquare\\Main.xml ExpressionLessSquare\\output\\Main.xml
echo Comparing Square.xml
TextComparer.bat ExpressionLessSquare\\Square.xml ExpressionLessSquare\\output\\Square.xml
echo Comparing SquareGame.xml
TextComparer.bat ExpressionLessSquare\\SquareGame.xml ExpressionLessSquare\\output\\SquareGame.xml
echo
echo
# Square ArrayTest
echo Tokenizing ArrayTest\\
python.exe jackanalyzer.py token_only diff_dir ArrayTest/
echo Comparing MainT.xml
TextComparer.bat ArrayTest\\MainT.xml ArrayTest\\output\\MainT.xml
echo

echo Analyzing ArrayTest\\
python.exe jackanalyzer.py no_token diff_dir ArrayTest/
echo Comparing Main.xml
TextComparer.bat ArrayTest\\Main.xml ArrayTest\\output\\Main.xml
echo
echo

# tabIssue
echo Tokenizing tabIssue\\
python.exe jackanalyzer.py no_token diff_dir tabIssue/
echo No tokenizer to compare!!
TextComparer.bat tabIssue\\tabIssue.xml tabIssue\\output\\tabIssue.xml
echo
# 1
echo Analyzing 1\\
python.exe jackanalyzer.py no_token diff_dir 1/
echo Comparing directory 1
 python.exe tests.py 1/
echo
echo
# 3
echo Analyzing 3\\
python.exe jackanalyzer.py no_token diff_dir 3/
echo Comparing directory 3
 python.exe tests.py 3/
echo
echo
# 5
echo Analyzing 5\\
python.exe jackanalyzer.py no_token diff_dir 5/
echo Comparing directory 5
 python.exe tests.py 5/
echo
echo
# Average
echo Analyzing Average\\
python.exe jackanalyzer.py no_token diff_dir Average/
echo Comparing directory Average
 python.exe tests.py Average/
echo
echo
# ComplexArrays
echo Analyzing ComplexArrays\\
python.exe jackanalyzer.py no_token diff_dir ComplexArrays/
echo Comparing directory ComplexArrays
 python.exe tests.py ComplexArrays/
echo
echo
# ConvertToBin
echo Analyzing ConvertToBin\\
python.exe jackanalyzer.py no_token diff_dir ConvertToBin/
echo Comparing directory ConvertToBin
 python.exe tests.py ConvertToBin/
echo
echo
# GASboing
echo Analyzing GASboing\\
python.exe jackanalyzer.py no_token diff_dir GASboing/
echo Comparing directory GASboing
 python.exe tests.py GASboing/
echo
echo
# JackScheme-master
echo Analyzing JackScheme-master\\
python.exe jackanalyzer.py no_token diff_dir JackScheme-master/
echo Comparing directory JackScheme-master
 python.exe tests.py JackScheme-master/
echo
echo
# Pong
echo Analyzing Pong\\
python.exe jackanalyzer.py no_token diff_dir Pong/
echo Comparing directory Pong
 python.exe tests.py Pong/
echo
echo
# Seven
echo Analyzing Seven\\
python.exe jackanalyzer.py no_token diff_dir Seven/
echo Comparing directory Seven
 python.exe tests.py Seven/
echo
echo
# sokoban
echo Analyzing sokoban\\
python.exe jackanalyzer.py no_token diff_dir sokoban/
echo Comparing directory sokoban
 python.exe tests.py sokoban/
echo
echo
# tictactoe-jack-master
echo Analyzing tictactoe-jack-master\\
python.exe jackanalyzer.py no_token diff_dir tictactoe-jack-master/
echo Comparing directory tictactoe-jack-master
 python.exe tests.py tictactoe-jack-master/
echo
echo
# Square11
echo Analyzing Square11\\
python.exe jackanalyzer.py no_token diff_dir JackScheme-Square11/
echo Comparing directory Square11
 python.exe tests.py Square11/
echo
echo


