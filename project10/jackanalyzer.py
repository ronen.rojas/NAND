import os
import sys
import re
from xml.etree.ElementTree import Element, SubElement, tostring
from xml.dom import minidom

NEW_LINE = '\n'


def prettify(elem):
    """
    Return a pretty-printed XML string for the Element.
    :param elem: The regular XML string (str)
    :return: The prettified XML string (str)
    """
    raw_string = tostring(elem)
    re_parsed_str = minidom.parseString(raw_string).toprettyxml(indent='\t')
    return re_parsed_str[re_parsed_str.index(NEW_LINE) + 1:]


def sub_elem(parent, node):
    """
    Create new sub-element with tag and text like child
    :param parent: Parent node (Element tree class)
    :param node: Copied node (Element tree class)
    :return: No return value
    """
    new_node = SubElement(parent, node.tag)
    if node.text:
        new_node.text = node.text
    else:
        new_node.text = NEW_LINE


def empty_elem(parent, tag):
    """
    Create new sub-element with tag and empty text
    :param parent: Parent node (Element tree class
    :param tag: Tag of the new node (str)
    :return: No return value
    """
    new_node = SubElement(parent, tag)
    new_node.text = NEW_LINE


class JackTokenizer:
    """
    This class removes all comments and white space from the input stream and
    breaks it into Jack language tokens, as specified by the Jack grammar.
    """
    KEYWORDS = ['class', 'constructor', 'function', 'method', 'field',
                'static', 'var', 'int', 'char', 'boolean', 'void', 'true',
                'false', 'null', 'this', 'let', 'do', 'if', 'else', 'while',
                'return']
    SYMBOLS = ('{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/',
               '&', '|', '<', '>', '=', '~')
    SYMBOLS_RE = '[' + re.escape(''.join(SYMBOLS)) + ']'
    KEYWORD_RE = '(?!\w)|'.join(KEYWORDS) + '(?!\w)'
    INTEGER_RE = r'\d+'
    STRING_RE = r'"[^"\n]*"'
    IDENTIFIER_RE = r'[\w]+'
    WORD_RE = re.compile(KEYWORD_RE + '|' + SYMBOLS_RE + '|' + INTEGER_RE +
                         '|' + STRING_RE + '|' + IDENTIFIER_RE)
    # State machine mode for code cleaning
    CODE = 0
    CONSTANT_STR = 1
    COMMENT_LINE = 2
    COMMENT_BLOCK = 3

    def __init__(self, jack_file):
        """
        Opens the input file/stream and tokenize it.
        :param jack_file: The input jack file (str)
        """
        # Read the jack file raw text code
        self.jack_text = open(jack_file, 'r').read()
        # Create the root of the token tree
        self.root = Element('tokens')
        # Clean all the comments
        self._clean_comments()
        # Tokenize every token
        self.tokenize()

    def __iter__(self):
        """
        Iterate for every token match.
        :return: Regular expressions matches in the Jack text.
        """
        return self.WORD_RE.finditer(self.jack_text)

    def get_xml_str(self):
        """
        Return the prettified XML text for the token tree
        :return: Prettified XML text (str)
        """
        return prettify(self.root)

    def _clean_comments(self):
        """
        Clean out comments in the jack code using state machine logic where
        code character that is not a code or a string constant gets taken off.
        :return: No return value
        """
        clean_str = ''
        idx = 0
        mode = self.CODE
        while idx < len(self.jack_text):
            if self.jack_text[idx] == '"':
                if mode == self.CODE:
                    mode = self.CONSTANT_STR
                elif mode == self.CONSTANT_STR:
                    mode = self.CODE
            if self.jack_text[idx] == '/':
                if mode == self.CODE:
                    if self.jack_text[idx + 1] == '/':
                        mode = self.COMMENT_LINE
                    elif self.jack_text[idx + 1] == '*':
                        mode = self.COMMENT_BLOCK
                if mode == self.COMMENT_BLOCK:
                    if self.jack_text[idx - 1] == '*':
                        mode = self.CODE
                        idx += 1
                        clean_str += ' '
                        continue
            if self.jack_text[idx] == NEW_LINE:
                if mode == self.COMMENT_LINE:
                    mode = self.CODE
            if mode == self.CODE or mode == self.CONSTANT_STR:
                clean_str += self.jack_text[idx]
            idx += 1
        self.jack_text = clean_str

    def write(self, xml_file):
        """
        Writes the tokens XML file
        :param xml_file: Name of the tokens XML file(str)
        :return: No return value
        """
        file_object = open(xml_file, 'w')
        file_object.write(prettify(self.root))
        file_object.close()

    def tokenize(self):
        """
        Tokenize everything according to the jack grammar
        :return: No return value
        """
        for match in self:
            word = match.group()
            token_elem = SubElement(self.root, self.tokenize_word(word))
            if self.tokenize_word(word) == 'stringConstant':
                word = word[1:-1]
            token_elem.text = word

    def tokenize_word(self, word):
        """
        Return the token for each work
        :param word: the word to be analyzed (str)
        :return: Token (str)
        """
        if re.match(self.KEYWORD_RE, word):
            return 'keyword'
        elif re.match(self.SYMBOLS_RE, word):
            return 'symbol'
        elif re.match(self.INTEGER_RE, word):
            return 'integerConstant'
        elif re.match(self.STRING_RE, word):
            return 'stringConstant'
        elif re.match(self.IDENTIFIER_RE, word):
            return 'identifier'


class CompilationEngine:
    """
    This class effects the actual compilation output. Gets its input from a
    JackTokenizer and emits its parsed structure into an output file/stream.
    The output is generated by a series of compile_xxx() methods, one for every
    syntactic element xxx of the Jack grammar. The contract between these
    methods is that each compile_xxx() method should read the syntactic
    construct xxx from the input, advance() the tokenizer exactly beyond xxx,
    and output the parsing of xxx. Thus, compile_xxx() may only be called if
    indeed xxx is the next syntactic element of the input.
    """
    CLASS_LEVEL_BAR_SYMBOL = ('keyword', 'identifier')
    CLASS_LEVEL_DEC = ('keyword', 'identifier', 'symbol')
    CLASS_VAR_DEC = ('field', 'static')
    SUBROUTINE_DEC = ('constructor', 'function', 'method')
    PARAMETER_OPEN_SYMBOL = '('
    PARAMETER_CLOSE_SYMBOL = ')'
    LEFT_CURLY_BRACKET = '{'
    RIGHT_CURLY_BRACKET = '}'
    LEFT_SQUARE_BRACKET = '['
    RIGHT_SQUARE_BRACKET = ']'
    STATEMENTS = ('do', 'let', 'while', 'return', 'if', 'else')
    EXPRESSION_START_SYMBOLS = (PARAMETER_OPEN_SYMBOL, '=',
                                LEFT_SQUARE_BRACKET)
    UNARY_OPERATORS = ('-', '~')
    BINARY_OPERATORS = ('-', '+', '*', '/', '&', '|', '<', '>', '=')
    CONSTANTS = ('integerConstant', 'stringConstant', 'keywordConstant')
    KEYWORD_CONSTANTS = ('true', 'false', 'null', 'this')
    COMMA = ','
    DOT = '.'
    SEMICOLON = ';'

    def __init__(self, jack_file):
        """
        Creates a new compilation engine with the given input and output.
        :param jack_file: The input jack file (str).
        """
        self.tokenizer = JackTokenizer(jack_file)
        self.xml_iterator = list(self.tokenizer.root.iter())
        # Initialize new tree
        self.cur_node_idx = 0
        self.new_root = Element('class')
        # Initialize flags
        self.depth_exp_list = False
        self.empty_param_list = False
        self.empty_expression_list = False
        self.build_tree()

    def build_tree(self):
        """
        Creates an XML Tree Top tier class - 0
        :return: No return value
        """
        while self.has_more_nodes():
            if self.cur_node().text == 'class':
                self.compile_class(self.new_root)
            self.advance()
        return

    def cur_node(self):
        """
        returns the current token being iterated upon.
        :return: No return value
        """
        return self.xml_iterator[self.cur_node_idx]

    def next_node(self):
        return self.xml_iterator[self.cur_node_idx + 1] if \
            self.has_more_nodes() else self.cur_node()

    def has_more_nodes(self):
        """
        checks if there are any tokens left to iterate upon.
        :return: No return value
        """
        return self.cur_node_idx + 1 < len(self.xml_iterator)

    def revert(self):
        """
        revert the token iterator by 1 index.
        :return: No return value
        """
        self.cur_node_idx -= 1

    def advance(self):
        """
        advance  the token iterator by 1 index.
        :return: No return value
        """
        self.cur_node_idx += 1

    def compile_class(self, parent):
        """
        Compile a class statement tier class - 1. Called by Build_tree.
        :param parent: parent node of the class
        :return: No return value
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.text in self.CLASS_VAR_DEC:
                self.revert()
                self.compile_class_var_dec(SubElement(parent, 'classVarDec'))
            elif child.text in self.SUBROUTINE_DEC:
                self.revert()
                self.compile_subroutine(SubElement(parent, 'subroutineDec'))
            elif child.tag in self.CLASS_LEVEL_DEC:
                sub_elem(parent, child)

    def compile_class_var_dec(self, parent):
        """
        Compile a class variable declaration. tier class - 2. Called by
        compile_class.
        :param parent:  parent node of the declaration
        :return: 'semi-recursive' return to sender: compile_class
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.tag in self.CLASS_LEVEL_DEC + (self.SEMICOLON,):
                sub_elem(parent, child)
                if child.text == self.SEMICOLON:
                    return
            else:
                raise Exception("Couldn't compile class variable declaration")

    def compile_subroutine(self, parent):
        """
        Compile a subroutine statement. Tier class - 2. Called by compile_class
        :param parent: parent node of the subroutine
        :return: 'semi-recursive' return to sender: compile_class
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.tag in self.CLASS_LEVEL_DEC:
                if child.text == self.RIGHT_CURLY_BRACKET:
                    if not self.has_more_nodes():
                        self.revert()
                    return
                sub_elem(parent, child)
                if child.text == self.PARAMETER_OPEN_SYMBOL:
                    self.empty_param_list = True
                    self.compile_parameter_list(SubElement(parent,
                                                           'parameterList'))
                elif child.text == self.PARAMETER_CLOSE_SYMBOL:
                    self.compile_subroutine_body(SubElement(parent,
                                                            'subroutineBody'))
            else:
                raise Exception("Couldn't compile subroutine declaration")

    def compile_subroutine_body(self, parent):
        """
        Compile the subroutine body tier class - 3. Called by
        compile_subroutine_dec
        :param parent: parent node of the subroutine
        :return:  'semi-recursive' return to sender: compile_subroutine_dec
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.tag == 'symbol':
                sub_elem(parent, child)
                if child.text == self.LEFT_CURLY_BRACKET and \
                   self.cur_node().text == self.RIGHT_CURLY_BRACKET:
                    empty_elem(parent, 'statements')
                if child.text == self.RIGHT_CURLY_BRACKET:
                    self.revert()
                    return
            elif child.tag == "keyword":
                if child.text not in self.STATEMENTS + ('var',):
                    sub_elem(parent, child)
                    continue
                self.revert()
                if child.text in self.STATEMENTS:
                    self.compile_statements(SubElement(parent, 'statements'))
                    self.revert()
                elif child.text == 'var':
                    self.compile_var_dec(SubElement(parent, 'varDec'))

    def compile_var_dec(self, parent):
        """
        Compile a variable declaration statement tier class - 4. Called by
        compile_subroutine_body.
        :param parent:  parent node of the subroutine
        :return:  'semi-recursive' return to sender: compile_subroutine_body
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.tag in self.CLASS_LEVEL_DEC:
                sub_elem(parent, child)
                if child.text == self.SEMICOLON:
                    return
            else:
                raise Exception("Couldn't compile variable declaration")

    def compile_parameter_list(self, parent):
        """
        Compile a parameter list tier class - 3. Called by
        compile_subroutine_dec
        :param parent:  parent node of the parameter list
        :return:  'semi-recursive' return to sender: compile_subroutine_dec
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.tag in self.CLASS_LEVEL_DEC:
                if child.text == self.PARAMETER_CLOSE_SYMBOL:
                    if self.empty_param_list:
                        parent.text = NEW_LINE
                    self.revert()
                    return
                self.empty_param_list = False
                sub_elem(parent, child)
            else:
                raise Exception("Couldn't compile parameter list")

    def compile_statements(self, parent):
        """
        Compile a number of statements tier class - 4. Called by
        compile_subroutine_body.
        :param parent: parent node of the statements
        :return: 'semi-recursive' return to sender: compile_subroutine_body
        """
        if self.cur_node().text == "}":  # empty statements
            parent.text = NEW_LINE
            self.advance()
            return
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.text in self.STATEMENTS:
                self.revert()
            if child.text == 'do':
                self.compile_do(SubElement(parent, 'doStatement'))
            elif child.text == 'return':
                self.compile_return(SubElement(parent, 'returnStatement'))
            elif child.text == 'let':
                self.compile_let(SubElement(parent, 'letStatement'))
            elif child.text == 'while':
                self.compile_while(SubElement(parent, 'whileStatement'))
            elif child.text == 'if':
                self.compile_if(SubElement(parent, 'ifStatement'))
            else:
                return

    def compile_do(self, parent):
        """
        Compile a 'do statement' tier class - 5. Called by compile_statements
        :param parent:  parent node of the statement
        """

        sub_elem(parent, self.cur_node())
        self.advance()
        self.compile_subroutine_call(parent)
        sub_elem(parent, self.cur_node())
        self.advance()

    def compile_let(self, parent):
        """
        Compile a 'let statement' tier class - 5. Called by compile_statements
        :param parent:  parent node of the statement
        """
        sub_elem(parent, self.cur_node())
        self.advance()
        sub_elem(parent, self.cur_node())
        self.advance()
        if self.cur_node().text == self.LEFT_SQUARE_BRACKET:
            sub_elem(parent, self.cur_node())
            self.advance()
            self.compile_expression(SubElement(parent, 'expression'))
            sub_elem(parent, self.cur_node())
            self.advance()
        sub_elem(parent, self.cur_node())  # =
        self.advance()
        self.compile_expression(SubElement(parent, 'expression'))
        sub_elem(parent, self.cur_node())
        self.advance()

    def compile_while(self, parent):
        """
        Compile a 'while statement' tier class - 5. Called by
        compile_statements
        :param parent:  parent node of the statement
        """
        sub_elem(parent, self.cur_node())
        self.advance()
        sub_elem(parent, self.cur_node())
        self.advance()
        self.compile_expression(SubElement(parent, 'expression'))
        sub_elem(parent, self.cur_node())
        self.advance()
        sub_elem(parent, self.cur_node())
        self.advance()
        self.compile_statements(SubElement(parent, 'statements'))
        self.revert()
        sub_elem(parent, self.cur_node())
        self.advance()

    def compile_return(self, parent):
        """
        Compile a 'return statement' tier class - 5. Called by
        compile_statements
        :param parent:  parent node of the statement
        """
        sub_elem(parent, self.cur_node())
        self.advance()
        if self.cur_node().text != self.SEMICOLON:
            self.compile_expression(SubElement(parent, 'expression'))
        sub_elem(parent, self.cur_node())
        self.advance()

    def compile_if(self, parent):
        """
        Compile an 'if statement' tier class - 5. Called by compile_statements
        :param parent:  parent node of the statement
        """
        sub_elem(parent, self.cur_node())  # if
        self.advance()
        sub_elem(parent, self.cur_node())  # (
        self.advance()
        self.compile_expression(SubElement(parent, 'expression'))
        sub_elem(parent, self.cur_node())  # )
        self.advance()
        sub_elem(parent, self.cur_node())  # {
        self.advance()
        self.compile_statements(SubElement(parent, 'statements'))
        self.revert()
        sub_elem(parent, self.cur_node())  # }
        self.advance()
        if self.cur_node().text == "else":
            self.compile_else(parent)

    def compile_else(self, parent):
        """
        Compile an 'else statement' tier class - 6. Called by
        compile_if
        :param parent:  parent node of the statement
        """
        sub_elem(parent, self.cur_node())  # else
        self.advance()
        sub_elem(parent, self.cur_node())  # {
        self.advance()
        self.compile_statements(SubElement(parent, 'statements'))
        self.revert()
        sub_elem(parent, self.cur_node())  # }
        self.advance()

    def compile_expression(self, parent):
        """
        Compile an expressions tier class - 6
        :param parent:  parent node of the expression
        :return:  'semi-recursive' return to sender:
        """
        self.compile_term(SubElement(parent, 'term'))
        while self.cur_node().text in self.BINARY_OPERATORS:
            sub_elem(parent, self.cur_node())
            self.advance()
            self.compile_term(SubElement(parent, 'term'))

    def compile_term(self, parent):
        """
        Compile a term tier class - 7. Called by compile_expression
        :param parent:  parent node of the term
        :return:  'semi-recursive' return to sender
        """
        #  integer, string, keyword constant
        if self.cur_node().tag in self.CONSTANTS or \
                self.cur_node().text in self.KEYWORD_CONSTANTS:
            sub_elem(parent, self.cur_node())
        # '(' expression ')'
        elif self.cur_node().text == self.PARAMETER_OPEN_SYMBOL:
            sub_elem(parent, self.cur_node())
            self.advance()
            self.compile_expression(SubElement(parent, 'expression'))
            sub_elem(parent, self.cur_node())
        # unary Op Term
        elif self.cur_node().text in self.UNARY_OPERATORS:
            sub_elem(parent, self.cur_node())
            self.advance()
            self.compile_term(SubElement(parent, 'term'))
            self.revert()
        # varName, subroutine call
        elif self.cur_node().tag == "identifier":
            # varName + [Expression]
            if self.next_node().text == self.LEFT_SQUARE_BRACKET:
                sub_elem(parent, self.cur_node())
                self.advance()
                sub_elem(parent, self.cur_node())
                self.advance()
                self.compile_expression(SubElement(parent, 'expression'))
                sub_elem(parent, self.cur_node())
            elif self.next_node().text == self.PARAMETER_OPEN_SYMBOL or \
                    self.next_node().text == self.DOT:
                self.compile_subroutine_call(parent)
                self.revert()
            else:
                sub_elem(parent, self.cur_node())
        self.advance()
        return

    def compile_subroutine_call(self, parent):
        """
        compile_subroutine_call
        :param parent: parent node of the expression
        :return:
        """
        sub_elem(parent, self.cur_node())
        self.advance()
        # expression List
        if self.cur_node().text == self.PARAMETER_OPEN_SYMBOL:
            sub_elem(parent, self.cur_node())
            self.advance()
            self.compile_expression_list(SubElement(parent, 'expressionList'))
            sub_elem(parent, self.cur_node())
        elif self.cur_node().text == self.DOT:
            sub_elem(parent, self.cur_node())
            self.advance()
            sub_elem(parent, self.cur_node())
            self.advance()
            sub_elem(parent, self.cur_node())
            self.advance()
            self.compile_expression_list(SubElement(parent, 'expressionList'))
            sub_elem(parent, self.cur_node())
        self.advance()

    def compile_expression_list(self, parent):
        """
        Compile an Expression List tier class - 8.
        :param parent:  parent node of the expression List
        :return:  'semi-recursive' return to sender:
        """
        while self.has_more_nodes():
            child = self.cur_node()
            self.advance()
            if child.tag == 'symbol' and child.text == \
                    self.PARAMETER_CLOSE_SYMBOL:
                parent.text = NEW_LINE
                self.revert()
                self.empty_expression_list = True
                return
            elif child.text == self.COMMA:
                sub_elem(parent, child)
            elif child.tag in self.CONSTANTS + self.CLASS_LEVEL_DEC:
                self.revert()
                self.depth_exp_list = True
                # Recursion into compile_expression and compile_term functions
                self.compile_expression(SubElement(parent, 'expression'))
                self.depth_exp_list = False
                if self.cur_node().text == self.COMMA:
                    continue
                return
            else:
                return

    def write(self, xml_file):
        """
        Write the tree that was built to an xml file.
        :param xml_file: the file location of the xml file
        """
        file_object = open(xml_file, 'w')
        file_object.write(prettify(self.new_root))
        file_object.close()


if __name__ == "__main__":
    # Check for the intermediate stage of only tokenizing
    token_flag = sys.argv[1] == 'token_only'
    # Output in the same directory
    same_dir_flag = sys.argv[2] == 'same_dir'
    # Input string from user
    input_str = sys.argv[3]
    if same_dir_flag:
        dir_output = input_str
    else:
        dir_output = os.path.join(os.path.dirname(input_str), 'output')
        # Creating output directory if not exist
        if not os.path.exists(dir_output):
            os.makedirs(dir_output)
    # Only tokenizing
    if token_flag:
        if os.path.isfile(input_str) and input_str.endswith('.jack'):
            # Tokenizing a single file
            jack_token = JackTokenizer(input_str)
            path, file = os.path.split(input_str)
            jack_token.write(
                os.path.join(dir_output, file.replace('.jack', 'T.xml')))
        elif os.path.isdir(input_str):
            # Tokenizing a directory
            for file in os.listdir(input_str):
                if file.endswith('.jack'):
                    jack_token = JackTokenizer(os.path.join(input_str, file))
                    jack_token.write(
                        os.path.join(dir_output,
                                     file.replace('.jack', 'T.xml')))
        else:
            print("No such input to tokenize")
    else:
        # Only Compiling
        if os.path.isfile(input_str)and input_str.endswith('.jack'):
            compile_engine = CompilationEngine(input_str)
            path, file = os.path.split(input_str)
            if same_dir_flag:
                file_path = input_str
            else:
                file_path = os.path.join(dir_output, file)
            compile_engine.write(file_path.replace('.jack', '.xml'))
        elif os.path.isdir(input_str):
            for file in os.listdir(input_str):
                file_path = os.path.join(input_str, file)
                if file_path.endswith('.jack') and os.path.isfile(file_path):
                    compile_engine = CompilationEngine(file_path)
                    if not same_dir_flag:
                        file_path = os.path.join(dir_output, file)
                    compile_engine.write(file_path.replace('.jack', '.xml'))
        else:
            print("No such input to compile")
