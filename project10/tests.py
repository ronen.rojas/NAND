import os
import sys
fmt_diff = 'diff -w {0} {1}'
fmt_echo = 'echo {0}'
if __name__ == '__main__':
    input_str = sys.argv[1]
    summary = []
    if os.path.isdir(input_str):
        for filename in os.listdir(sys.argv[1]):
            if filename.endswith(".xml"):
                out_dir = os.path.join(os.path.dirname(input_str), 'output')
                xml_ref = os.path.join(input_str, filename)
                xml_res = os.path.join(out_dir, filename)
                if os.path.isfile(xml_res):
                    res_diff = os.system(fmt_diff.format(xml_ref, xml_res))
                    if res_diff != 0:
                        summary.append(xml_ref)
        print("Summary: " + str(len(summary)) + " tests failed\n")
        for file_n in summary:
            print("In file: " + file_n + " there were diffs\n")

    else:
        print("No directory was found")
