#!/bin/bash
echo Runing test5.sh
./test5.sh
echo

echo ===================================================
echo Done running test5.sh
echo ===================================================
echo

FILE_LIST='CPU Computer Memory ExtendAlu CpuMul'

echo Testing CPU.hdl
HardwareSimulator.bat CPU.tst
echo

echo Testing Computer.hdl on ComputerAdd.tst
HardwareSimulator.bat ComputerAdd.tst
echo
echo Testing Computer.hdl on ComputerMax.tst
HardwareSimulator.bat ComputerMax.tst
echo
echo Testing Computer.hdl on ComputerRect.tst
HardwareSimulator.bat ComputerRect.tst
echo

echo Testing ExtendAlu.hdl 
HardwareSimulator.bat ExtendAlu.tst
echo

echo Testing CpuMul.hdl 
HardwareSimulator.bat CpuMul.tst
echo

echo Can not Test Memory.hdl , test it on HardwareSimulator
