/**
* This chip is an extension of the book CPU by using the extended ALU.
* More specificly if instruction[15]==0 or (instruction[14] and instruction[13] equals 1)
* the CpuMul behave exactly as the book CPU.
* While if it is C instruction and instruction[13] == 0 the output will be D*A/M 
* (according to instruction[12]).
* Moreover, if it is c instruction and instruction[14] == 0 it will behave as follows:
*
* instruction:  | 12 | 11 | 10 |
* _____________________________
* shift left D  | 0  | 1  | 1  |
* shift left A  | 0  | 1  | 0  |
* shift left M  | 1  | 1  | 0  |
* shift right D | 0  | 0  | 1  |
* shift right A | 0  | 0  | 0  |
* shift right M | 1  | 0  | 0  |
**/

CHIP CpuMul{

    IN  inM[16],         // M value input  (M = contents of RAM[A])
        instruction[16], // Instruction for execution
        reset;           // Signals whether to re-start the current
                         // program (reset=1) or continue executing
                         // the current program (reset=0).

    OUT outM[16],        // M value output
        writeM,          // Write into M? 
        addressM[15],    // Address in data memory (of M)
        pc[15];          // address of next instruction

    PARTS:
	 
	// Writing to M in decided by d3 = True
	And(a=instruction[3], b=instruction[15], out=writeM);
	
	// Select if it's A register or M 
	Mux16(a=outFromAreg, b=inM, sel=instruction[12], out=AorMInputToAlu);
	
	// Compute the ExtendAlu side
	ExtendAlu(x=DInputToAlu, y=AorMInputToAlu,
			  instruction=instruction[6..14],
		      out=outFromAlu1 ,out=outFromAlu2 , out=outM, 
		      zr=zrFromAlu1, zr=zrFromAlu2, 
		      ng=ngFromALu1, ng=ngFromALu2);
	
	// A - register
	// Decode whether if it's an A-instruction or C-instruction with destination to A (d1=True)
	Not(in=instruction[15], out=flagInstructionA);
	Or(a=flagInstructionA, b=instruction[5], out=loadToAreg);
	
	// Choose whether if it's an A-instruction or C-instruction with destination to A
	Mux16(a=instruction, b=outFromAlu1, sel=instruction[15], out=inToAreg);
	ARegister(in=inToAreg, load=loadToAreg, out=outFromAreg, out[0..14]=addressM, out=outFromAregtoPC);
	
	// D - register
	// Decode whether if it's an C-instruction with destination to D (d2=True) and not C-instruction 
	And(a=instruction[4], b=instruction[15], out=loadToDreg);
	DRegister(in=outFromAlu2, load=loadToDreg, out=DInputToAlu);
	
	// Next Instruction
	Not(in=zrFromAlu2, out=notzrFromAlu);
	Not(in=ngFromALu2, out=notngFromALu);
	And(a=notzrFromAlu, b=notngFromALu, out=outFromAlupos);
	And(a=instruction[2], b=ngFromALu1, out=flagJ1);
	And(a=instruction[1], b=zrFromAlu1, out=flagJ2);
	And(a=instruction[0], b=outFromAlupos, out=flagJ3);
	
	// Calc flag jump
	Or8Way(in[0]=flagJ1, in[1]=flagJ2, in[2]=flagJ3, out=flagjumpTotal);
	// Jump only for C instructions
	And(a=flagjumpTotal, b=instruction[15], out=loadToPC);
	PC(in=outFromAregtoPC, load=loadToPC, inc=true, reset=reset, out[0..14]=pc);
}
