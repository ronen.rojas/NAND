#!/bin/bash

# Math
\cp  Math.jack  MathTest/Math.jack
JackCompiler.bat MathTest
echo Testing MathTest
VMEmulator.bat MathTest/MathTest.tst
echo

# Array
\cp  Array.jack  ArrayTest/Array.jack
JackCompiler.bat ArrayTest
echo Testing ArrayTest
VMEmulator.bat ArrayTest/ArrayTest.tst
echo

# Memory
\cp  Memory.jack  MemoryTest/Memory.jack
JackCompiler.bat MemoryTest
echo Testing MemoryTest
VMEmulator.bat MemoryTest/MemoryTest.tst
echo

# Screen
\cp  Screen.jack  ScreenTest/Screen.jack
JackCompiler.bat ScreenTest
echo open  VMEmulator and test ScreenTest
echo

# Keyboard
\cp  Keyboard.jack  KeyboardTest/Keyboard.jack
JackCompiler.bat KeyboardTest
echo open  VMEmulator and test KeyboardTest
echo

# Sys
\cp  Sys.jack  SysTest/Sys.jack
JackCompiler.bat SysTest
echo open  VMEmulator and test SysTest
echo

# Output
\cp  Output.jack  OutputTest/Output.jack
JackCompiler.bat OutputTest
echo open  VMEmulator and test OutputTest

# String
\cp  String.jack  StringTest/String.jack
JackCompiler.bat StringTest
echo open  VMEmulator and test StringTest

