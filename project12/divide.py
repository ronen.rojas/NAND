

def bit(y, j):
    return y & 2**j

q_times_y_twice = 0


def divide(x, y):
    global q_times_y_twice
    if y > x or y < 0:
        q_times_y_twice = 0
        return 0
    q = divide(x, y + y)
    if (x - q_times_y_twice) < y:
        return q + q
    else:
        q_times_y_twice += y
        return q + q + 1


print(divide(3434443, 34553))
print(3434443//34553)


