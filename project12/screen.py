baseScreenAddress = 16384
lastScreenAddress = 24576

def calcAddress(x,y):
    return baseScreenAddress + y*32 + x // 16

print(calcAddress(174, 60))
print(calcAddress(194, 60))
print(calcAddress(106, 60))
print(calcAddress(86, 60))

