#!/bin/bash
echo Running test1.sh
./test1.sh
echo
FILE_LIST='And DMux DMux8Way Mux16 Mux8Way16 Not16 Or16 Xor And16 DMux4Way Mux Mux4Way16 Not Or Or8Way'
for CHIP in $FILE_LIST; do
  if [ ! -r $CHIP.hdl ]; then
    echo Chip $CHIP was not found
  else
	echo Testing $CHIP.hdl Chip on Hardware Emulator:
	 HardwareSimulator.bat $CHIP.tst
	echo 
  fi
done
