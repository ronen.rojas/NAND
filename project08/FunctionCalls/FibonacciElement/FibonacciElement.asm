@256
D=A
@SP
M=D
@Sys.init0
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(Sys.init0)

// Translating FunctionCalls/FibonacciElement\Main.vm


// function Main.fibonacci 0

(Main.fibonacci)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK1
D;JLT
@SP
A=M-1
D=M
@TRUE1
	D;JLT
@START1
0;JMP
(CHECK1)
@SP
A=M-1
D=M
@FALSE1
D;JGE
(START1)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE1
	D;JLT
	@FALSE1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
	@SKIP1
	0;JMP
(FALSE1)
	@SP
	A=M-1
	M=0
(SKIP1)

// if-goto IF_TRUE

@SP
AM=M-1
D=M
@Main.fibonacci$IF_TRUE
D;JNE

// goto IF_FALSE

@Main.fibonacci$IF_FALSE
0;JMP

// label IF_TRUE

(Main.fibonacci$IF_TRUE)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// label IF_FALSE

(Main.fibonacci$IF_FALSE)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// call Main.fibonacci 1

@Main.fibonacci2
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(Main.fibonacci2)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// call Main.fibonacci 1

@Main.fibonacci3
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(Main.fibonacci3)

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating FunctionCalls/FibonacciElement\Sys.vm


// function Sys.init 0

(Sys.init)

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// call Main.fibonacci 1

@Main.fibonacci4
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(Main.fibonacci4)

// label WHILE

(Sys.init$WHILE)

// goto WHILE

@Sys.init$WHILE
0;JMP
