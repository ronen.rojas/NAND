
// Translating Tests/overflowTest/test.vm


// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK0
D;JGT
@SP
A=M-1
D=M
@TRUE0
	D;JGT
@START0
0;JMP
(CHECK0)
@SP
A=M-1
D=M
@FALSE0
D;JLE
(START0)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE0
	D;JGT
	@FALSE0
	0;JMP
(TRUE0)
	@SP
	A=M-1
	M=-1
	@SKIP0
	0;JMP
(FALSE0)
	@SP
	A=M-1
	M=0
(SKIP0)

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE1
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
(SKIP1)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK2
D;JLT
@SP
A=M-1
D=M
@TRUE2
	D;JLT
@START2
0;JMP
(CHECK2)
@SP
A=M-1
D=M
@FALSE2
D;JGE
(START2)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE2
	D;JLT
	@FALSE2
	0;JMP
(TRUE2)
	@SP
	A=M-1
	M=-1
	@SKIP2
	0;JMP
(FALSE2)
	@SP
	A=M-1
	M=0
(SKIP2)

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK3
D;JGT
@SP
A=M-1
D=M
@TRUE3
	D;JGT
@START3
0;JMP
(CHECK3)
@SP
A=M-1
D=M
@FALSE3
D;JLE
(START3)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE3
	D;JGT
	@FALSE3
	0;JMP
(TRUE3)
	@SP
	A=M-1
	M=-1
	@SKIP3
	0;JMP
(FALSE3)
	@SP
	A=M-1
	M=0
(SKIP3)
