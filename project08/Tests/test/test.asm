@256
D=A
@SP
M=D
@Sys.init0
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(Sys.init0)

// Translating project8/Tests/test\Array.vm


// function Array.new 0

(Array.new)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK1
D;JGT
@SP
A=M-1
D=M
@TRUE1
	D;JGT
@START1
0;JMP
(CHECK1)
@SP
A=M-1
D=M
@FALSE1
D;JLE
(START1)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE1
	D;JGT
	@FALSE1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
	@SKIP1
	0;JMP
(FALSE1)
	@SP
	A=M-1
	M=0
(SKIP1)

// not

@SP
A=M-1
M=!M

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Array.new$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Array.new$IF_FALSE0
0;JMP

// label IF_TRUE0

(Array.new$IF_TRUE0)

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error2
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error2)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Array.new$IF_FALSE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.alloc 1

@Memory.alloc3
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.alloc
0;JMP
(Memory.alloc3)

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Array.dispose 0

(Array.dispose)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// push pointer 0

@THIS
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.deAlloc 1

@Memory.deAlloc4
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.deAlloc
0;JMP
(Memory.deAlloc4)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating project8/Tests/test\Main.vm


// function Main.main 12

(Main.main)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 3

@3
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 2

@2
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 3

@3
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 5

@5
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 4

@4
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 6

@6
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 5

@5
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 4

@4
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 5

@5
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 6

@6
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.multiply 2

@Math.multiply5
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.multiply
0;JMP
(Math.multiply5)

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.multiply 2

@Math.multiply6
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.multiply
0;JMP
(Math.multiply6)

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 4

@4
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 5

@5
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.multiply 2

@Math.multiply7
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.multiply
0;JMP
(Math.multiply7)

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 7

@7
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 5

@5
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.divide 2

@Math.divide8
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.divide
0;JMP
(Math.divide8)

// push local 4

@4
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.divide 2

@Math.divide9
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.divide
0;JMP
(Math.divide9)

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.divide 2

@Math.divide10
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.divide
0;JMP
(Math.divide10)

// pop local 8

@8
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 5000

@5000
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 6

@6
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.poke 2

@Memory.poke11
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.poke
0;JMP
(Memory.poke11)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 5001

@5001
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 7

@7
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.poke 2

@Memory.poke12
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.poke
0;JMP
(Memory.poke12)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 5002

@5002
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 8

@8
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.poke 2

@Memory.poke13
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.poke
0;JMP
(Memory.poke13)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating project8/Tests/test\Math.vm


// function Math.init 1

(Math.init)
@SP
AM=M+1
A=A-1
M=0

// push constant 16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// call Array.new 1

@Array.new14
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Array.new
0;JMP
(Array.new14)

// pop static 1

@SP
AM=M-1
D=M
@Math.1
M=D

// push constant 16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// call Array.new 1

@Array.new15
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Array.new
0;JMP
(Array.new15)

// pop static 0

@SP
AM=M-1
D=M
@Math.0
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP0

(Math.init$WHILE_EXP0)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 15

@15
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK16
D;JLT
@SP
A=M-1
D=M
@TRUE16
	D;JLT
@START16
0;JMP
(CHECK16)
@SP
A=M-1
D=M
@FALSE16
D;JGE
(START16)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE16
	D;JLT
	@FALSE16
	0;JMP
(TRUE16)
	@SP
	A=M-1
	M=-1
	@SKIP16
	0;JMP
(FALSE16)
	@SP
	A=M-1
	M=0
(SKIP16)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Math.init$WHILE_END0
D;JNE

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Math.init$WHILE_EXP0
0;JMP

// label WHILE_END0

(Math.init$WHILE_END0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Math.abs 0

(Math.abs)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK17
D;JLT
@SP
A=M-1
D=M
@TRUE17
	D;JLT
@START17
0;JMP
(CHECK17)
@SP
A=M-1
D=M
@FALSE17
D;JGE
(START17)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE17
	D;JLT
	@FALSE17
	0;JMP
(TRUE17)
	@SP
	A=M-1
	M=-1
	@SKIP17
	0;JMP
(FALSE17)
	@SP
	A=M-1
	M=0
(SKIP17)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Math.abs$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Math.abs$IF_FALSE0
0;JMP

// label IF_TRUE0

(Math.abs$IF_TRUE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE0

(Math.abs$IF_FALSE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Math.multiply 5

(Math.multiply)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK18
D;JLT
@SP
A=M-1
D=M
@TRUE18
	D;JLT
@START18
0;JMP
(CHECK18)
@SP
A=M-1
D=M
@FALSE18
D;JGE
(START18)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE18
	D;JLT
	@FALSE18
	0;JMP
(TRUE18)
	@SP
	A=M-1
	M=-1
	@SKIP18
	0;JMP
(FALSE18)
	@SP
	A=M-1
	M=0
(SKIP18)

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK19
D;JGT
@SP
A=M-1
D=M
@TRUE19
	D;JGT
@START19
0;JMP
(CHECK19)
@SP
A=M-1
D=M
@FALSE19
D;JLE
(START19)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE19
	D;JGT
	@FALSE19
	0;JMP
(TRUE19)
	@SP
	A=M-1
	M=-1
	@SKIP19
	0;JMP
(FALSE19)
	@SP
	A=M-1
	M=0
(SKIP19)

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK20
D;JGT
@SP
A=M-1
D=M
@TRUE20
	D;JGT
@START20
0;JMP
(CHECK20)
@SP
A=M-1
D=M
@FALSE20
D;JLE
(START20)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE20
	D;JGT
	@FALSE20
	0;JMP
(TRUE20)
	@SP
	A=M-1
	M=-1
	@SKIP20
	0;JMP
(FALSE20)
	@SP
	A=M-1
	M=0
(SKIP20)

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK21
D;JLT
@SP
A=M-1
D=M
@TRUE21
	D;JLT
@START21
0;JMP
(CHECK21)
@SP
A=M-1
D=M
@FALSE21
D;JGE
(START21)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE21
	D;JLT
	@FALSE21
	0;JMP
(TRUE21)
	@SP
	A=M-1
	M=-1
	@SKIP21
	0;JMP
(FALSE21)
	@SP
	A=M-1
	M=0
(SKIP21)

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// or

@SP
AM=M-1
D=M
A=A-1
M=D|M

// pop local 4

@4
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.abs 1

@Math.abs22
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.abs
0;JMP
(Math.abs22)

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.abs 1

@Math.abs23
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.abs
0;JMP
(Math.abs23)

// pop argument 1

@1
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK24
D;JLT
@SP
A=M-1
D=M
@TRUE24
	D;JLT
@START24
0;JMP
(CHECK24)
@SP
A=M-1
D=M
@FALSE24
D;JGE
(START24)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE24
	D;JLT
	@FALSE24
	0;JMP
(TRUE24)
	@SP
	A=M-1
	M=-1
	@SKIP24
	0;JMP
(FALSE24)
	@SP
	A=M-1
	M=0
(SKIP24)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Math.multiply$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Math.multiply$IF_FALSE0
0;JMP

// label IF_TRUE0

(Math.multiply$IF_TRUE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop argument 1

@1
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE0

(Math.multiply$IF_FALSE0)

// label WHILE_EXP0

(Math.multiply$WHILE_EXP0)

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK25
D;JLT
@SP
A=M-1
D=M
@TRUE25
	D;JLT
@START25
0;JMP
(CHECK25)
@SP
A=M-1
D=M
@FALSE25
D;JGE
(START25)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE25
	D;JLT
	@FALSE25
	0;JMP
(TRUE25)
	@SP
	A=M-1
	M=-1
	@SKIP25
	0;JMP
(FALSE25)
	@SP
	A=M-1
	M=0
(SKIP25)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Math.multiply$WHILE_END0
D;JNE

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK26
D;JGT
@SP
A=M-1
D=M
@TRUE26
	D;JGT
@START26
0;JMP
(CHECK26)
@SP
A=M-1
D=M
@FALSE26
D;JLE
(START26)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE26
	D;JGT
	@FALSE26
	0;JMP
(TRUE26)
	@SP
	A=M-1
	M=-1
	@SKIP26
	0;JMP
(FALSE26)
	@SP
	A=M-1
	M=0
(SKIP26)

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Math.multiply$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Math.multiply$IF_FALSE1
0;JMP

// label IF_TRUE1

(Math.multiply$IF_TRUE1)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 2

@2
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE1

(Math.multiply$IF_FALSE1)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 3

@3
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Math.multiply$WHILE_EXP0
0;JMP

// label WHILE_END0

(Math.multiply$WHILE_END0)

// push local 4

@4
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// if-goto IF_TRUE2

@SP
AM=M-1
D=M
@Math.multiply$IF_TRUE2
D;JNE

// goto IF_FALSE2

@Math.multiply$IF_FALSE2
0;JMP

// label IF_TRUE2

(Math.multiply$IF_TRUE2)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE2

(Math.multiply$IF_FALSE2)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Math.divide 4

(Math.divide)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE27
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP27
	0;JMP
(TRUE27)
	@SP
	A=M-1
	M=-1
(SKIP27)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Math.divide$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Math.divide$IF_FALSE0
0;JMP

// label IF_TRUE0

(Math.divide$IF_TRUE0)

// push constant 3

@3
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error28
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error28)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Math.divide$IF_FALSE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK29
D;JLT
@SP
A=M-1
D=M
@TRUE29
	D;JLT
@START29
0;JMP
(CHECK29)
@SP
A=M-1
D=M
@FALSE29
D;JGE
(START29)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE29
	D;JLT
	@FALSE29
	0;JMP
(TRUE29)
	@SP
	A=M-1
	M=-1
	@SKIP29
	0;JMP
(FALSE29)
	@SP
	A=M-1
	M=0
(SKIP29)

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK30
D;JGT
@SP
A=M-1
D=M
@TRUE30
	D;JGT
@START30
0;JMP
(CHECK30)
@SP
A=M-1
D=M
@FALSE30
D;JLE
(START30)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE30
	D;JGT
	@FALSE30
	0;JMP
(TRUE30)
	@SP
	A=M-1
	M=-1
	@SKIP30
	0;JMP
(FALSE30)
	@SP
	A=M-1
	M=0
(SKIP30)

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK31
D;JGT
@SP
A=M-1
D=M
@TRUE31
	D;JGT
@START31
0;JMP
(CHECK31)
@SP
A=M-1
D=M
@FALSE31
D;JLE
(START31)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE31
	D;JGT
	@FALSE31
	0;JMP
(TRUE31)
	@SP
	A=M-1
	M=-1
	@SKIP31
	0;JMP
(FALSE31)
	@SP
	A=M-1
	M=0
(SKIP31)

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK32
D;JLT
@SP
A=M-1
D=M
@TRUE32
	D;JLT
@START32
0;JMP
(CHECK32)
@SP
A=M-1
D=M
@FALSE32
D;JGE
(START32)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE32
	D;JLT
	@FALSE32
	0;JMP
(TRUE32)
	@SP
	A=M-1
	M=-1
	@SKIP32
	0;JMP
(FALSE32)
	@SP
	A=M-1
	M=0
(SKIP32)

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// or

@SP
AM=M-1
D=M
A=A-1
M=D|M

// pop local 2

@2
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.abs 1

@Math.abs33
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.abs
0;JMP
(Math.abs33)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Math.abs 1

@Math.abs34
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.abs
0;JMP
(Math.abs34)

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP0

(Math.divide$WHILE_EXP0)

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Math.divide$WHILE_END0
D;JNE

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK35
D;JLT
@SP
A=M-1
D=M
@TRUE35
	D;JLT
@START35
0;JMP
(CHECK35)
@SP
A=M-1
D=M
@FALSE35
D;JGE
(START35)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE35
	D;JLT
	@FALSE35
	0;JMP
(TRUE35)
	@SP
	A=M-1
	M=-1
	@SKIP35
	0;JMP
(FALSE35)
	@SP
	A=M-1
	M=0
(SKIP35)

// pop local 3

@3
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Math.divide$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Math.divide$IF_FALSE1
0;JMP

// label IF_TRUE1

(Math.divide$IF_TRUE1)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK36
D;JGT
@SP
A=M-1
D=M
@TRUE36
	D;JGT
@START36
0;JMP
(CHECK36)
@SP
A=M-1
D=M
@FALSE36
D;JLE
(START36)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE36
	D;JGT
	@FALSE36
	0;JMP
(TRUE36)
	@SP
	A=M-1
	M=-1
	@SKIP36
	0;JMP
(FALSE36)
	@SP
	A=M-1
	M=0
(SKIP36)

// pop local 3

@3
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// if-goto IF_TRUE2

@SP
AM=M-1
D=M
@Math.divide$IF_TRUE2
D;JNE

// goto IF_FALSE2

@Math.divide$IF_FALSE2
0;JMP

// label IF_TRUE2

(Math.divide$IF_TRUE2)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE2

(Math.divide$IF_FALSE2)

// label IF_FALSE1

(Math.divide$IF_FALSE1)

// goto WHILE_EXP0

@Math.divide$WHILE_EXP0
0;JMP

// label WHILE_END0

(Math.divide$WHILE_END0)

// label WHILE_EXP1

(Math.divide$WHILE_EXP1)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK37
D;JGT
@SP
A=M-1
D=M
@TRUE37
	D;JGT
@START37
0;JMP
(CHECK37)
@SP
A=M-1
D=M
@FALSE37
D;JLE
(START37)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE37
	D;JGT
	@FALSE37
	0;JMP
(TRUE37)
	@SP
	A=M-1
	M=-1
	@SKIP37
	0;JMP
(FALSE37)
	@SP
	A=M-1
	M=0
(SKIP37)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END1

@SP
AM=M-1
D=M
@Math.divide$WHILE_END1
D;JNE

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK38
D;JGT
@SP
A=M-1
D=M
@TRUE38
	D;JGT
@START38
0;JMP
(CHECK38)
@SP
A=M-1
D=M
@FALSE38
D;JLE
(START38)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE38
	D;JGT
	@FALSE38
	0;JMP
(TRUE38)
	@SP
	A=M-1
	M=-1
	@SKIP38
	0;JMP
(FALSE38)
	@SP
	A=M-1
	M=0
(SKIP38)

// not

@SP
A=M-1
M=!M

// if-goto IF_TRUE3

@SP
AM=M-1
D=M
@Math.divide$IF_TRUE3
D;JNE

// goto IF_FALSE3

@Math.divide$IF_FALSE3
0;JMP

// label IF_TRUE3

(Math.divide$IF_TRUE3)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 1

@Math.1
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE3

(Math.divide$IF_FALSE3)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP1

@Math.divide$WHILE_EXP1
0;JMP

// label WHILE_END1

(Math.divide$WHILE_END1)

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// if-goto IF_TRUE4

@SP
AM=M-1
D=M
@Math.divide$IF_TRUE4
D;JNE

// goto IF_FALSE4

@Math.divide$IF_FALSE4
0;JMP

// label IF_TRUE4

(Math.divide$IF_TRUE4)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE4

(Math.divide$IF_FALSE4)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Math.sqrt 2

(Math.sqrt)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK39
D;JLT
@SP
A=M-1
D=M
@TRUE39
	D;JLT
@START39
0;JMP
(CHECK39)
@SP
A=M-1
D=M
@FALSE39
D;JGE
(START39)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE39
	D;JLT
	@FALSE39
	0;JMP
(TRUE39)
	@SP
	A=M-1
	M=-1
	@SKIP39
	0;JMP
(FALSE39)
	@SP
	A=M-1
	M=0
(SKIP39)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Math.sqrt$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Math.sqrt$IF_FALSE0
0;JMP

// label IF_TRUE0

(Math.sqrt$IF_TRUE0)

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error40
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error40)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Math.sqrt$IF_FALSE0)

// push constant 7

@7
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP0

(Math.sqrt$WHILE_EXP0)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK41
D;JGT
@SP
A=M-1
D=M
@TRUE41
	D;JGT
@START41
0;JMP
(CHECK41)
@SP
A=M-1
D=M
@FALSE41
D;JLE
(START41)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE41
	D;JGT
	@FALSE41
	0;JMP
(TRUE41)
	@SP
	A=M-1
	M=-1
	@SKIP41
	0;JMP
(FALSE41)
	@SP
	A=M-1
	M=0
(SKIP41)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Math.sqrt$WHILE_END0
D;JNE

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// call Math.multiply 2

@Math.multiply42
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.multiply
0;JMP
(Math.multiply42)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK43
D;JGT
@SP
A=M-1
D=M
@TRUE43
	D;JGT
@START43
0;JMP
(CHECK43)
@SP
A=M-1
D=M
@FALSE43
D;JLE
(START43)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE43
	D;JGT
	@FALSE43
	0;JMP
(TRUE43)
	@SP
	A=M-1
	M=-1
	@SKIP43
	0;JMP
(FALSE43)
	@SP
	A=M-1
	M=0
(SKIP43)

// not

@SP
A=M-1
M=!M

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Math.sqrt$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Math.sqrt$IF_FALSE1
0;JMP

// label IF_TRUE1

(Math.sqrt$IF_TRUE1)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Math.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE1

(Math.sqrt$IF_FALSE1)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Math.sqrt$WHILE_EXP0
0;JMP

// label WHILE_END0

(Math.sqrt$WHILE_END0)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Math.max 0

(Math.max)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK44
D;JGT
@SP
A=M-1
D=M
@TRUE44
	D;JGT
@START44
0;JMP
(CHECK44)
@SP
A=M-1
D=M
@FALSE44
D;JLE
(START44)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE44
	D;JGT
	@FALSE44
	0;JMP
(TRUE44)
	@SP
	A=M-1
	M=-1
	@SKIP44
	0;JMP
(FALSE44)
	@SP
	A=M-1
	M=0
(SKIP44)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Math.max$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Math.max$IF_FALSE0
0;JMP

// label IF_TRUE0

(Math.max$IF_TRUE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop argument 1

@1
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE0

(Math.max$IF_FALSE0)

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Math.min 0

(Math.min)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK45
D;JLT
@SP
A=M-1
D=M
@TRUE45
	D;JLT
@START45
0;JMP
(CHECK45)
@SP
A=M-1
D=M
@FALSE45
D;JGE
(START45)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE45
	D;JLT
	@FALSE45
	0;JMP
(TRUE45)
	@SP
	A=M-1
	M=-1
	@SKIP45
	0;JMP
(FALSE45)
	@SP
	A=M-1
	M=0
(SKIP45)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Math.min$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Math.min$IF_FALSE0
0;JMP

// label IF_TRUE0

(Math.min$IF_TRUE0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop argument 1

@1
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE0

(Math.min$IF_FALSE0)

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating project8/Tests/test\Memory.vm


// function Memory.init 0

(Memory.init)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// pop static 0

@SP
AM=M-1
D=M
@Memory.0
M=D

// push constant 2048

@2048
D=A
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 14334

@14334
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 2049

@2049
D=A
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 2050

@2050
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.peek 0

(Memory.peek)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.poke 0

(Memory.poke)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.alloc 2

(Memory.alloc)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK46
D;JLT
@SP
A=M-1
D=M
@TRUE46
	D;JLT
@START46
0;JMP
(CHECK46)
@SP
A=M-1
D=M
@FALSE46
D;JGE
(START46)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE46
	D;JLT
	@FALSE46
	0;JMP
(TRUE46)
	@SP
	A=M-1
	M=-1
	@SKIP46
	0;JMP
(FALSE46)
	@SP
	A=M-1
	M=0
(SKIP46)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Memory.alloc$IF_FALSE0
0;JMP

// label IF_TRUE0

(Memory.alloc$IF_TRUE0)

// push constant 5

@5
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error47
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error47)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Memory.alloc$IF_FALSE0)

// push constant 2048

@2048
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP0

(Memory.alloc$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK48
D;JLT
@SP
A=M-1
D=M
@TRUE48
	D;JLT
@START48
0;JMP
(CHECK48)
@SP
A=M-1
D=M
@FALSE48
D;JGE
(START48)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE48
	D;JLT
	@FALSE48
	0;JMP
(TRUE48)
	@SP
	A=M-1
	M=-1
	@SKIP48
	0;JMP
(FALSE48)
	@SP
	A=M-1
	M=0
(SKIP48)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Memory.alloc$WHILE_END0
D;JNE

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Memory.alloc$WHILE_EXP0
0;JMP

// label WHILE_END0

(Memory.alloc$WHILE_END0)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 16379

@16379
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK49
D;JGT
@SP
A=M-1
D=M
@TRUE49
	D;JGT
@START49
0;JMP
(CHECK49)
@SP
A=M-1
D=M
@FALSE49
D;JLE
(START49)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE49
	D;JGT
	@FALSE49
	0;JMP
(TRUE49)
	@SP
	A=M-1
	M=-1
	@SKIP49
	0;JMP
(FALSE49)
	@SP
	A=M-1
	M=0
(SKIP49)

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Memory.alloc$IF_FALSE1
0;JMP

// label IF_TRUE1

(Memory.alloc$IF_TRUE1)

// push constant 6

@6
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error50
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error50)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE1

(Memory.alloc$IF_FALSE1)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// gt

@SP
AM=M-1
D=M
@CHECK51
D;JGT
@SP
A=M-1
D=M
@TRUE51
	D;JGT
@START51
0;JMP
(CHECK51)
@SP
A=M-1
D=M
@FALSE51
D;JLE
(START51)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE51
	D;JGT
	@FALSE51
	0;JMP
(TRUE51)
	@SP
	A=M-1
	M=-1
	@SKIP51
	0;JMP
(FALSE51)
	@SP
	A=M-1
	M=0
(SKIP51)

// if-goto IF_TRUE2

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE2
D;JNE

// goto IF_FALSE2

@Memory.alloc$IF_FALSE2
0;JMP

// label IF_TRUE2

(Memory.alloc$IF_TRUE2)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE52
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP52
	0;JMP
(TRUE52)
	@SP
	A=M-1
	M=-1
(SKIP52)

// if-goto IF_TRUE3

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE3
D;JNE

// goto IF_FALSE3

@Memory.alloc$IF_FALSE3
0;JMP

// label IF_TRUE3

(Memory.alloc$IF_TRUE3)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 3

@3
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto IF_END3

@Memory.alloc$IF_END3
0;JMP

// label IF_FALSE3

(Memory.alloc$IF_FALSE3)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 3

@3
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_END3

(Memory.alloc$IF_END3)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE2

(Memory.alloc$IF_FALSE2)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.deAlloc 2

(Memory.deAlloc)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE53
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP53
	0;JMP
(TRUE53)
	@SP
	A=M-1
	M=-1
(SKIP53)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Memory.deAlloc$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Memory.deAlloc$IF_FALSE0
0;JMP

// label IF_TRUE0

(Memory.deAlloc$IF_TRUE0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto IF_END0

@Memory.deAlloc$IF_END0
0;JMP

// label IF_FALSE0

(Memory.deAlloc$IF_FALSE0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE54
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP54
	0;JMP
(TRUE54)
	@SP
	A=M-1
	M=-1
(SKIP54)

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Memory.deAlloc$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Memory.deAlloc$IF_FALSE1
0;JMP

// label IF_TRUE1

(Memory.deAlloc$IF_TRUE1)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto IF_END1

@Memory.deAlloc$IF_END1
0;JMP

// label IF_FALSE1

(Memory.deAlloc$IF_FALSE1)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_END1

(Memory.deAlloc$IF_END1)

// label IF_END0

(Memory.deAlloc$IF_END0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating project8/Tests/test\Sys.vm


// function Sys.init 0

(Sys.init)

// call Memory.init 0

@Memory.init55
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.init
0;JMP
(Memory.init55)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// call Math.init 0

@Math.init56
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Math.init
0;JMP
(Math.init56)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// call Main.main 0

@Main.main57
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.main
0;JMP
(Main.main57)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label WHILE_EXP0

(Sys.init$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.init$WHILE_END0
D;JNE

// goto WHILE_EXP0

@Sys.init$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.init$WHILE_END0)

// function Sys.halt 0

(Sys.halt)

// label WHILE_EXP0

(Sys.halt$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.halt$WHILE_END0
D;JNE

// goto WHILE_EXP0

@Sys.halt$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.halt$WHILE_END0)

// function Sys.wait 1

(Sys.wait)
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK58
D;JLT
@SP
A=M-1
D=M
@TRUE58
	D;JLT
@START58
0;JMP
(CHECK58)
@SP
A=M-1
D=M
@FALSE58
D;JGE
(START58)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE58
	D;JLT
	@FALSE58
	0;JMP
(TRUE58)
	@SP
	A=M-1
	M=-1
	@SKIP58
	0;JMP
(FALSE58)
	@SP
	A=M-1
	M=0
(SKIP58)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Sys.wait$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Sys.wait$IF_FALSE0
0;JMP

// label IF_TRUE0

(Sys.wait$IF_TRUE0)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error59
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error59)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Sys.wait$IF_FALSE0)

// label WHILE_EXP0

(Sys.wait$WHILE_EXP0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK60
D;JGT
@SP
A=M-1
D=M
@TRUE60
	D;JGT
@START60
0;JMP
(CHECK60)
@SP
A=M-1
D=M
@FALSE60
D;JLE
(START60)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE60
	D;JGT
	@FALSE60
	0;JMP
(TRUE60)
	@SP
	A=M-1
	M=-1
	@SKIP60
	0;JMP
(FALSE60)
	@SP
	A=M-1
	M=0
(SKIP60)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.wait$WHILE_END0
D;JNE

// push constant 50

@50
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP1

(Sys.wait$WHILE_EXP1)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK61
D;JGT
@SP
A=M-1
D=M
@TRUE61
	D;JGT
@START61
0;JMP
(CHECK61)
@SP
A=M-1
D=M
@FALSE61
D;JLE
(START61)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE61
	D;JGT
	@FALSE61
	0;JMP
(TRUE61)
	@SP
	A=M-1
	M=-1
	@SKIP61
	0;JMP
(FALSE61)
	@SP
	A=M-1
	M=0
(SKIP61)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END1

@SP
AM=M-1
D=M
@Sys.wait$WHILE_END1
D;JNE

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP1

@Sys.wait$WHILE_EXP1
0;JMP

// label WHILE_END1

(Sys.wait$WHILE_END1)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Sys.wait$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.wait$WHILE_END0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Sys.error 0

(Sys.error)

// label WHILE_EXP0

(Sys.error$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.error$WHILE_END0
D;JNE

// goto WHILE_EXP0

@Sys.error$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.error$WHILE_END0)
