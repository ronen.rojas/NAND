@256
D=A
@SP
M=D
@Sys.init0
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(Sys.init0)

// Translating Tests/test4\Main.vm


// function Main.main 0

(Main.main)

// call Stata.init 0

@Stata.init1
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Stata.init
0;JMP
(Stata.init1)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// call Statb.init 0

@Statb.init2
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Statb.init
0;JMP
(Statb.init2)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 5

@5
D=A
@SP
AM=M+1
A=A-1
M=D

// call Main.f 1

@Main.f3
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.f
0;JMP
(Main.f3)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Main.f 0

(Main.f)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// call Main.dump 1

@Main.dump4
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.dump
0;JMP
(Main.dump4)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Stata.setVal 1

@Stata.setVal5
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Stata.setVal
0;JMP
(Stata.setVal5)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// call Statb.setVal 1

@Statb.setVal6
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Statb.setVal
0;JMP
(Statb.setVal6)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 10

@10
D=A
@SP
AM=M+1
A=A-1
M=D

// call Main.dump 1

@Main.dump7
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.dump
0;JMP
(Main.dump7)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Main.dump 3

(Main.dump)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// call Stata.getVal 0

@Stata.getVal8
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Stata.getVal
0;JMP
(Stata.getVal8)

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// call Statb.getVal 0

@Statb.getVal9
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Statb.getVal
0;JMP
(Statb.getVal9)

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 5000

@5000
D=A
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.poke 2

@Memory.poke10
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.poke
0;JMP
(Memory.poke10)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 5001

@5001
D=A
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// call Memory.poke 2

@Memory.poke11
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@2
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.poke
0;JMP
(Memory.poke11)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/test4\Memory.vm


// function Memory.init 0

(Memory.init)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// pop static 0

@SP
AM=M-1
D=M
@Memory.0
M=D

// push constant 2048

@2048
D=A
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 14334

@14334
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 2049

@2049
D=A
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 2050

@2050
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.peek 0

(Memory.peek)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.poke 0

(Memory.poke)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push static 0

@Memory.0
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.alloc 2

(Memory.alloc)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK12
D;JLT
@SP
A=M-1
D=M
@TRUE12
	D;JLT
@START12
0;JMP
(CHECK12)
@SP
A=M-1
D=M
@FALSE12
D;JGE
(START12)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE12
	D;JLT
	@FALSE12
	0;JMP
(TRUE12)
	@SP
	A=M-1
	M=-1
	@SKIP12
	0;JMP
(FALSE12)
	@SP
	A=M-1
	M=0
(SKIP12)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Memory.alloc$IF_FALSE0
0;JMP

// label IF_TRUE0

(Memory.alloc$IF_TRUE0)

// push constant 5

@5
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error13
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error13)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Memory.alloc$IF_FALSE0)

// push constant 2048

@2048
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP0

(Memory.alloc$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK14
D;JLT
@SP
A=M-1
D=M
@TRUE14
	D;JLT
@START14
0;JMP
(CHECK14)
@SP
A=M-1
D=M
@FALSE14
D;JGE
(START14)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE14
	D;JLT
	@FALSE14
	0;JMP
(TRUE14)
	@SP
	A=M-1
	M=-1
	@SKIP14
	0;JMP
(FALSE14)
	@SP
	A=M-1
	M=0
(SKIP14)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Memory.alloc$WHILE_END0
D;JNE

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Memory.alloc$WHILE_EXP0
0;JMP

// label WHILE_END0

(Memory.alloc$WHILE_END0)

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 16379

@16379
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK15
D;JGT
@SP
A=M-1
D=M
@TRUE15
	D;JGT
@START15
0;JMP
(CHECK15)
@SP
A=M-1
D=M
@FALSE15
D;JLE
(START15)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE15
	D;JGT
	@FALSE15
	0;JMP
(TRUE15)
	@SP
	A=M-1
	M=-1
	@SKIP15
	0;JMP
(FALSE15)
	@SP
	A=M-1
	M=0
(SKIP15)

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Memory.alloc$IF_FALSE1
0;JMP

// label IF_TRUE1

(Memory.alloc$IF_TRUE1)

// push constant 6

@6
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error16
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error16)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE1

(Memory.alloc$IF_FALSE1)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// gt

@SP
AM=M-1
D=M
@CHECK17
D;JGT
@SP
A=M-1
D=M
@TRUE17
	D;JGT
@START17
0;JMP
(CHECK17)
@SP
A=M-1
D=M
@FALSE17
D;JLE
(START17)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE17
	D;JGT
	@FALSE17
	0;JMP
(TRUE17)
	@SP
	A=M-1
	M=-1
	@SKIP17
	0;JMP
(FALSE17)
	@SP
	A=M-1
	M=0
(SKIP17)

// if-goto IF_TRUE2

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE2
D;JNE

// goto IF_FALSE2

@Memory.alloc$IF_FALSE2
0;JMP

// label IF_TRUE2

(Memory.alloc$IF_TRUE2)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE18
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP18
	0;JMP
(TRUE18)
	@SP
	A=M-1
	M=-1
(SKIP18)

// if-goto IF_TRUE3

@SP
AM=M-1
D=M
@Memory.alloc$IF_TRUE3
D;JNE

// goto IF_FALSE3

@Memory.alloc$IF_FALSE3
0;JMP

// label IF_TRUE3

(Memory.alloc$IF_TRUE3)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 3

@3
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto IF_END3

@Memory.alloc$IF_END3
0;JMP

// label IF_FALSE3

(Memory.alloc$IF_FALSE3)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 3

@3
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_END3

(Memory.alloc$IF_END3)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_FALSE2

(Memory.alloc$IF_FALSE2)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Memory.deAlloc 2

(Memory.deAlloc)
@SP
AM=M+1
A=A-1
M=0
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE19
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP19
	0;JMP
(TRUE19)
	@SP
	A=M-1
	M=-1
(SKIP19)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Memory.deAlloc$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Memory.deAlloc$IF_FALSE0
0;JMP

// label IF_TRUE0

(Memory.deAlloc$IF_TRUE0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto IF_END0

@Memory.deAlloc$IF_END0
0;JMP

// label IF_FALSE0

(Memory.deAlloc$IF_FALSE0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE20
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP20
	0;JMP
(TRUE20)
	@SP
	A=M-1
	M=-1
(SKIP20)

// if-goto IF_TRUE1

@SP
AM=M-1
D=M
@Memory.deAlloc$IF_TRUE1
D;JNE

// goto IF_FALSE1

@Memory.deAlloc$IF_FALSE1
0;JMP

// label IF_TRUE1

(Memory.deAlloc$IF_TRUE1)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto IF_END1

@Memory.deAlloc$IF_END1
0;JMP

// label IF_FALSE1

(Memory.deAlloc$IF_FALSE1)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push temp 0

@R5
D=M
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label IF_END1

(Memory.deAlloc$IF_END1)

// label IF_END0

(Memory.deAlloc$IF_END0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/test4\Stata.vm


// function Stata.init 0

(Stata.init)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// pop static 0

@SP
AM=M-1
D=M
@Stata.0
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Stata.setVal 0

(Stata.setVal)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop static 0

@SP
AM=M-1
D=M
@Stata.0
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Stata.getVal 0

(Stata.getVal)

// push static 0

@Stata.0
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/test4\Statb.vm


// function Statb.init 0

(Statb.init)

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// pop static 0

@SP
AM=M-1
D=M
@Statb.0
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Statb.setVal 0

(Statb.setVal)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop static 0

@SP
AM=M-1
D=M
@Statb.0
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Statb.getVal 0

(Statb.getVal)

// push static 0

@Statb.0
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/test4\Sys.vm


// function Sys.init 0

(Sys.init)

// call Memory.init 0

@Memory.init21
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Memory.init
0;JMP
(Memory.init21)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// call Main.main 0

@Main.main22
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.main
0;JMP
(Main.main22)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label WHILE_EXP0

(Sys.init$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.init$WHILE_END0
D;JNE

// goto WHILE_EXP0

@Sys.init$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.init$WHILE_END0)

// function Sys.halt 0

(Sys.halt)

// label WHILE_EXP0

(Sys.halt$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.halt$WHILE_END0
D;JNE

// goto WHILE_EXP0

@Sys.halt$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.halt$WHILE_END0)

// function Sys.wait 1

(Sys.wait)
@SP
AM=M+1
A=A-1
M=0

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK23
D;JLT
@SP
A=M-1
D=M
@TRUE23
	D;JLT
@START23
0;JMP
(CHECK23)
@SP
A=M-1
D=M
@FALSE23
D;JGE
(START23)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE23
	D;JLT
	@FALSE23
	0;JMP
(TRUE23)
	@SP
	A=M-1
	M=-1
	@SKIP23
	0;JMP
(FALSE23)
	@SP
	A=M-1
	M=0
(SKIP23)

// if-goto IF_TRUE0

@SP
AM=M-1
D=M
@Sys.wait$IF_TRUE0
D;JNE

// goto IF_FALSE0

@Sys.wait$IF_FALSE0
0;JMP

// label IF_TRUE0

(Sys.wait$IF_TRUE0)

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// call Sys.error 1

@Sys.error24
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@1
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.error
0;JMP
(Sys.error24)

// pop temp 0

@SP
AM=M-1
D=M
@R5
M=D

// label IF_FALSE0

(Sys.wait$IF_FALSE0)

// label WHILE_EXP0

(Sys.wait$WHILE_EXP0)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK25
D;JGT
@SP
A=M-1
D=M
@TRUE25
	D;JGT
@START25
0;JMP
(CHECK25)
@SP
A=M-1
D=M
@FALSE25
D;JLE
(START25)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE25
	D;JGT
	@FALSE25
	0;JMP
(TRUE25)
	@SP
	A=M-1
	M=-1
	@SKIP25
	0;JMP
(FALSE25)
	@SP
	A=M-1
	M=0
(SKIP25)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.wait$WHILE_END0
D;JNE

// push constant 50

@50
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE_EXP1

(Sys.wait$WHILE_EXP1)

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK26
D;JGT
@SP
A=M-1
D=M
@TRUE26
	D;JGT
@START26
0;JMP
(CHECK26)
@SP
A=M-1
D=M
@FALSE26
D;JLE
(START26)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE26
	D;JGT
	@FALSE26
	0;JMP
(TRUE26)
	@SP
	A=M-1
	M=-1
	@SKIP26
	0;JMP
(FALSE26)
	@SP
	A=M-1
	M=0
(SKIP26)

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END1

@SP
AM=M-1
D=M
@Sys.wait$WHILE_END1
D;JNE

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP1

@Sys.wait$WHILE_EXP1
0;JMP

// label WHILE_END1

(Sys.wait$WHILE_END1)

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// goto WHILE_EXP0

@Sys.wait$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.wait$WHILE_END0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// function Sys.error 0

(Sys.error)

// label WHILE_EXP0

(Sys.error$WHILE_EXP0)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// not

@SP
A=M-1
M=!M

// if-goto WHILE_END0

@SP
AM=M-1
D=M
@Sys.error$WHILE_END0
D;JNE

// goto WHILE_EXP0

@Sys.error$WHILE_EXP0
0;JMP

// label WHILE_END0

(Sys.error$WHILE_END0)
