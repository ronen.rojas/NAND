@256
D=A
@SP
M=D
@Sys.init0
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@0
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(Sys.init0)

// Translating Tests/OverFlowTests/OverFlowTests\OverFlowTestsEQ.vm


// function OverFlowTestsEQ.eqTest 1

(OverFlowTestsEQ.eqTest)
@SP
AM=M+1
A=A-1
M=0

// push constant 3000

@3000
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE1
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
(SKIP1)

// pop this 0

@0
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 0

@0
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE2
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP2
	0;JMP
(TRUE2)
	@SP
	A=M-1
	M=-1
(SKIP2)

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 1

@1
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE3
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP3
	0;JMP
(TRUE3)
	@SP
	A=M-1
	M=-1
(SKIP3)

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 2

@2
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE4
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP4
	0;JMP
(TRUE4)
	@SP
	A=M-1
	M=-1
(SKIP4)

// pop this 3

@3
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 3

@3
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE5
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP5
	0;JMP
(TRUE5)
	@SP
	A=M-1
	M=-1
(SKIP5)

// pop this 4

@4
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 4

@4
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/OverFlowTests/OverFlowTests\OverFlowTestsGT.vm


// function OverFlowTestsGT.gtTest 1

(OverFlowTestsGT.gtTest)
@SP
AM=M+1
A=A-1
M=0

// push constant 3005

@3005
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK6
D;JGT
@SP
A=M-1
D=M
@TRUE6
	D;JGT
@START6
0;JMP
(CHECK6)
@SP
A=M-1
D=M
@FALSE6
D;JLE
(START6)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE6
	D;JGT
	@FALSE6
	0;JMP
(TRUE6)
	@SP
	A=M-1
	M=-1
	@SKIP6
	0;JMP
(FALSE6)
	@SP
	A=M-1
	M=0
(SKIP6)

// pop this 0

@0
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 0

@0
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK7
D;JGT
@SP
A=M-1
D=M
@TRUE7
	D;JGT
@START7
0;JMP
(CHECK7)
@SP
A=M-1
D=M
@FALSE7
D;JLE
(START7)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE7
	D;JGT
	@FALSE7
	0;JMP
(TRUE7)
	@SP
	A=M-1
	M=-1
	@SKIP7
	0;JMP
(FALSE7)
	@SP
	A=M-1
	M=0
(SKIP7)

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 1

@1
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK8
D;JGT
@SP
A=M-1
D=M
@TRUE8
	D;JGT
@START8
0;JMP
(CHECK8)
@SP
A=M-1
D=M
@FALSE8
D;JLE
(START8)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE8
	D;JGT
	@FALSE8
	0;JMP
(TRUE8)
	@SP
	A=M-1
	M=-1
	@SKIP8
	0;JMP
(FALSE8)
	@SP
	A=M-1
	M=0
(SKIP8)

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 2

@2
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK9
D;JGT
@SP
A=M-1
D=M
@TRUE9
	D;JGT
@START9
0;JMP
(CHECK9)
@SP
A=M-1
D=M
@FALSE9
D;JLE
(START9)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE9
	D;JGT
	@FALSE9
	0;JMP
(TRUE9)
	@SP
	A=M-1
	M=-1
	@SKIP9
	0;JMP
(FALSE9)
	@SP
	A=M-1
	M=0
(SKIP9)

// pop this 3

@3
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 3

@3
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK10
D;JGT
@SP
A=M-1
D=M
@TRUE10
	D;JGT
@START10
0;JMP
(CHECK10)
@SP
A=M-1
D=M
@FALSE10
D;JLE
(START10)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE10
	D;JGT
	@FALSE10
	0;JMP
(TRUE10)
	@SP
	A=M-1
	M=-1
	@SKIP10
	0;JMP
(FALSE10)
	@SP
	A=M-1
	M=0
(SKIP10)

// pop this 4

@4
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 4

@4
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/OverFlowTests/OverFlowTests\OverFlowTestsLT.vm


// function OverFlowTestsLT.ltTest 1

(OverFlowTestsLT.ltTest)
@SP
AM=M+1
A=A-1
M=0

// push constant 3010

@3010
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK11
D;JLT
@SP
A=M-1
D=M
@TRUE11
	D;JLT
@START11
0;JMP
(CHECK11)
@SP
A=M-1
D=M
@FALSE11
D;JGE
(START11)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE11
	D;JLT
	@FALSE11
	0;JMP
(TRUE11)
	@SP
	A=M-1
	M=-1
	@SKIP11
	0;JMP
(FALSE11)
	@SP
	A=M-1
	M=0
(SKIP11)

// pop this 0

@0
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 0

@0
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK12
D;JLT
@SP
A=M-1
D=M
@TRUE12
	D;JLT
@START12
0;JMP
(CHECK12)
@SP
A=M-1
D=M
@FALSE12
D;JGE
(START12)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE12
	D;JLT
	@FALSE12
	0;JMP
(TRUE12)
	@SP
	A=M-1
	M=-1
	@SKIP12
	0;JMP
(FALSE12)
	@SP
	A=M-1
	M=0
(SKIP12)

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 1

@1
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK13
D;JLT
@SP
A=M-1
D=M
@TRUE13
	D;JLT
@START13
0;JMP
(CHECK13)
@SP
A=M-1
D=M
@FALSE13
D;JGE
(START13)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE13
	D;JLT
	@FALSE13
	0;JMP
(TRUE13)
	@SP
	A=M-1
	M=-1
	@SKIP13
	0;JMP
(FALSE13)
	@SP
	A=M-1
	M=0
(SKIP13)

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 2

@2
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK14
D;JLT
@SP
A=M-1
D=M
@TRUE14
	D;JLT
@START14
0;JMP
(CHECK14)
@SP
A=M-1
D=M
@FALSE14
D;JGE
(START14)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE14
	D;JLT
	@FALSE14
	0;JMP
(TRUE14)
	@SP
	A=M-1
	M=-1
	@SKIP14
	0;JMP
(FALSE14)
	@SP
	A=M-1
	M=0
(SKIP14)

// pop this 3

@3
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 3

@3
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK15
D;JLT
@SP
A=M-1
D=M
@TRUE15
	D;JLT
@START15
0;JMP
(CHECK15)
@SP
A=M-1
D=M
@FALSE15
D;JGE
(START15)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE15
	D;JLT
	@FALSE15
	0;JMP
(TRUE15)
	@SP
	A=M-1
	M=-1
	@SKIP15
	0;JMP
(FALSE15)
	@SP
	A=M-1
	M=0
(SKIP15)

// pop this 4

@4
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 4

@4
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// return

@LCL
D=M
@R14
M=D
@5
D=A
@R14
A=M-D
D=M
@R15
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@R14
A=M-1
D=M
@THAT
M=D
@2
D=A
@R14
A=M-D
D=M
@THIS
M=D
@3
D=A
@R14
A=M-D
D=M
@ARG
M=D
@4
D=A
@R14
A=M-D
D=M
@LCL
M=D
@R15
A=M
0;JMP

// Translating Tests/OverFlowTests/OverFlowTests\Sys.vm


// function Sys.init 0

(Sys.init)

// push constant 3015

@3015
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 16384

@16384
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// call OverFlowTestsGT.gtTest 4

@OverFlowTestsGT.gtTest16
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@4
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@OverFlowTestsGT.gtTest
0;JMP
(OverFlowTestsGT.gtTest16)

// pop this 0

@0
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 16384

@16384
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// call OverFlowTestsLT.ltTest 4

@OverFlowTestsLT.ltTest17
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@4
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@OverFlowTestsLT.ltTest
0;JMP
(OverFlowTestsLT.ltTest17)

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 16384

@16384
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// call OverFlowTestsEQ.eqTest 4

@OverFlowTestsEQ.eqTest18
D=A
@SP
AM=M+1
A=A-1
M=D
@LCL
D=M
@SP
AM=M+1
A=A-1
M=D
@ARG
D=M
@SP
AM=M+1
A=A-1
M=D
@THIS
D=M
@SP
AM=M+1
A=A-1
M=D
@THAT
D=M
@SP
AM=M+1
A=A-1
M=D
@4
D=A
@SP
D=M-D
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@OverFlowTestsEQ.eqTest
0;JMP
(OverFlowTestsEQ.eqTest18)

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// label WHILE

(Sys.init$WHILE)

// goto WHILE              // loops infinitely

@Sys.init$WHILE
0;JMP
