#!/bin/bash
cp ../project07/vmtranslator.py .

echo Translating  ProgramFlow/BasicLoop/BasicLoop.vm
python.exe vmtranslator.py no_bootstrap ProgramFlow/BasicLoop/BasicLoop.vm
CPUEmulator.bat ProgramFlow/BasicLoop/BasicLoop.tst
echo

echo Translating  ProgramFlow/FibonacciSeries/FibonacciSeries.vm
python.exe vmtranslator.py no_bootstrap ProgramFlow/FibonacciSeries/FibonacciSeries.vm
CPUEmulator.bat ProgramFlow/FibonacciSeries/FibonacciSeries.tst
echo

echo Translating  FunctionCalls/SimpleFunction/SimpleFunction.vm
python.exe vmtranslator.py no_bootstrap FunctionCalls/SimpleFunction/SimpleFunction.vm
CPUEmulator.bat FunctionCalls/SimpleFunction/SimpleFunction.tst
echo

echo Translating  FunctionCalls/NestedCall/
python.exe vmtranslator.py no_bootstrap FunctionCalls/NestedCall
CPUEmulator.bat FunctionCalls/NestedCall/NestedCall.tst
echo

echo Translating  FunctionCalls/FibonacciElement
python.exe vmtranslator.py bootstrap FunctionCalls/FibonacciElement
CPUEmulator.bat FunctionCalls/FibonacciElement/FibonacciElement.tst
echo

echo Translating  FunctionCalls/StaticsTest
python.exe vmtranslator.py bootstrap FunctionCalls/StaticsTest
CPUEmulator.bat FunctionCalls/StaticsTest/StaticsTest.tst
echo

echo Translating  Tests/overflowTest/test.vm
python.exe vmtranslator.py no_bootstrap Tests/overflowTest/test.vm
CPUEmulator.bat Tests/overflowTest/test.tst
echo

echo Translating  Tests/OverFlowTests/OverFlowTests
python.exe vmtranslator.py bootstrap Tests/OverFlowTests/OverFlowTests
CPUEmulator.bat  Tests/OverFlowTests/OverFlowTests/OverFlowTests.tst
echo

echo Translating  Tests/test3/
python.exe vmtranslator.py bootstrap Tests/test3
CPUEmulator.bat Tests/test3/test3.tst
echo

echo Translating  Tests/test4/
python.exe vmtranslator.py bootstrap Tests/test4
CPUEmulator.bat Tests/test4/test4.tst
echo


rm -f vmtranslator.py 