import sys
import os
import re


class Assembler:
    """
    This class encapsulates creating the hack file from an asm file.
    1st stage : Build a symbol table.
    2nd stage : Open a hack file and line by line translate it to binary code.
    """
    A_INSTRUCTION_PREFIX = '0'
    C_INSTRUCTION_PREFIX = '1'
    PRE_DEFINED_SYMBOLS = {'SP': 0, 'LCL': 1, 'ARG': 2, 'THIS': 3, 'THAT': 4,
                           'R0': 0, 'R1': 1, 'R2': 2, 'R3': 3, 'R4': 4,
                           'R5': 5, 'R6': 6, 'R7': 7, 'R8': 8, 'R9': 9,
                           'R10': 10,  'R11': 11, 'R12': 12, 'R13': 13,
                           'R14': 14, 'R15': 15, 'SCREEN': 16384, 'KBD': 24576}
    VARIABLE_MEMORY_IDX_START = 16

    def __init__(self, asm_file):
        """
        Constructor of the assembler class
        :param asm_file: Name of the asm file to translate (str)
        """
        # Code and parser objects
        self.code = Code()
        self.parser = Parser(asm_file)

        # Symbol table dictionary
        self.symbol_table = {}
        self.variable_dx = self.VARIABLE_MEMORY_IDX_START
        # Hack file id
        self.file_id = open(asm_file.replace('.asm', '.hack'), 'w')

    def __getitem__(self, idx):
        return self.symbol_table[idx]

    def __setitem__(self, key, value):
        self.symbol_table[key] = value

    def __contains__(self, item):
        if item in self.symbol_table:
            return True
        else:
            return False

    def symbol_stage(self):
        """
        1st stage : Build a symbol table.
        Goes through the entire assembly program, line by line, and build the
        symbol table without generating any code
        :return: No return value
        """
        idx_table = 0
        while self.parser.has_more_commands():
            self.parser.advance()
            if self.parser.command_type() == self.parser.L_COMMAND:
                if not self.parser.symbol() in self.PRE_DEFINED_SYMBOLS:
                    if not self.parser.symbol().isnumeric():
                        self[self.parser.symbol()] = idx_table

            else:
                idx_table += 1

    def symbol_less_stage(self):
        """
        2nd stage : Open a hack file and line by line translate it to binary
        code.
        Goes again through the entire program, and parse each line
        :return: No return value
        """
        while self.parser.has_more_commands():
            self.parser.advance()
            if self.parser.command_type() == self.parser.C_COMMAND:
                self._write_c_instruction()
            elif self.parser.command_type() == self.parser.A_COMMAND:
                self._write_a_instruction(self.parser.symbol())
        self.file_id.close()

    def _write_c_instruction(self):
        """
        Writes a C instruction
        :return: No return value
        """
        self.file_id.write(self.C_INSTRUCTION_PREFIX +
                           self.code.comp(self.parser.comp()) +
                           self.code.dest(self.parser.dest()) +
                           self.code.jump(self.parser.jump()) + '\n')

    def _write_a_instruction(self, symbol):
        """
        Writes an A instruction
        :param symbol: Name of the symbol to translate (str)
        :return: No return value
        """
        if symbol in self.PRE_DEFINED_SYMBOLS:
            mem_symbol = self.PRE_DEFINED_SYMBOLS[symbol]
        elif symbol in self:
            mem_symbol = self[symbol]
        elif not symbol.isnumeric():
            self[symbol] = self.variable_dx
            mem_symbol = self[symbol]
            self.variable_dx += 1
        else:
            mem_symbol = int(symbol)
        self.file_id.write(self.A_INSTRUCTION_PREFIX +
                           format(mem_symbol, '015b') + '\n')

    def assemble(self):
        """"
        Assembles all in one.
        :return: No return value
        """
        self.symbol_stage()
        self.parser.reset()
        self.symbol_less_stage()


class Parser:
    """
    This class encapsulates access to the input code. Reads an assembly
    language command, parses it, and provides convenient access to the
    command's components (fields and symbols). In addition, removes all
    white space and comments.
    """
    COMMENT_PREFIX = "//"
    COMMENT_RE = re.compile('//.*\n')
    A_COMMAND = 0
    C_COMMAND = 1
    L_COMMAND = 2

    def __init__(self, input_file):
        """
        Opens the input file/stream and gets ready to parse it
        :param input_file: Name of the asm file to translate (str)
        """
        # Opens the input file getting rid of comments and blank lines
        self.lines_list = [line.replace('\t', '').replace(' ', '') for line in
                           self._clean_comment(input_file) if line.strip()]
        self.current_index = -1

    def __getitem__(self, item):
        return self.lines_list[item]

    def _clean_comment(self, asm_file):
        """
        Striping comments.
        :return: No return value
        """
        return self.COMMENT_RE.sub('\n', open(asm_file).read()).split('\n')

    def reset(self):
        """
        Resetting the current command to the start.
        :return: No return value
        """
        self.current_index = -1

    def has_more_commands(self):
        """
        Are there more commands in the input?
        :return: Boolean - True has more commands False otherwise
        """
        return self.current_index + 1 < len(self.lines_list)

    def advance(self):
        """
        Sets the next command from the input and makes it the current
        command. Should be called only if hasMoreCommands() is true.
        Initially there is no current command
        :return: No return value
        """
        self.current_index += 1

    def command_type(self):
        """
        Returns the type of the current command:
            A_COMMAND - for @Xxx where Xxx is either a symbol or a decimal
                        number
            C_COMMAND - for dest=comp;jump
            L_COMMAND - (actually, pseudo-command) for (Xxx) where Xxx is a
                        symbol
        :return: Instruction type (int)
        """
        if self[self.current_index].startswith('@'):
            return self.A_COMMAND
        elif self[self.current_index].startswith('('):
            return self.L_COMMAND
        else:
            return self.C_COMMAND

    def symbol(self):
        """
        Returns the symbol or decimal Xxx of the current command @Xxx or
        (Xxx). Should be called only when commandType() is A_COMMAND or
        L_COMMAND
        :return: A/L instruction symbol (str)
        """
        out_str = self[self.current_index].replace('@', '').replace('(', '')
        return out_str.replace(')', '')

    def dest(self):
        """
        Returns the dest mnemonic in the current C-command (8 possibilities).
        Should be called only when commandType() is C_COMMAND.
        :return: C instruction destination (str)
        """
        return self[self.current_index].rpartition('=')[0]

    def comp(self):
        """
        Returns the comp mnemonic in the current C-command (28 possibilities).
        Should be called only when commandType() is C_COMMAND.
        :return: C instruction compute flag (str)
        """
        compute_str = self[self.current_index].rpartition('=')[-1]
        return compute_str.partition(';')[0]

    def jump(self):
        """
        Returns the jump mnemonic in the current C-command (8 possibilities).
        Should be called only when commandType() is C_COMMAND.
        :return: C instruction junp flag (str)
        """
        return self[self.current_index].partition(';')[-1]


class Code:
    """
    Translates Hack assembly language mnemonics into binary codes.
    This is a more class that is wrapping and encapsulates the binary
    machine language.
    """
    COMPUTE_TABLE = {'0': '110101010', '1': '110111111', '-1': '110111010',
                     'D': '110001100', 'A': '110110000', '!D': '110001101',
                     '!A': '110110001', '-D': '110001111', '-A': '110110011',
                     'D+1': '110011111', 'A+1': '110110111',
                     'D-1': '110001110', 'A-1': '110110010',
                     'D+A': '110000010', 'D-A': '110010011',
                     'A-D': '110000111', 'D&A': '110000000',
                     'D|A': '110010101', 'M': '111110000', '!M': '111110001',
                     '-M': '111110011', 'M+1': '111110111', 'M-1': '111110010',
                     'D+M': '111000010', 'D-M': '111010011',
                     'M-D': '111000111', 'D&M': '111000000',
                     'D|M': '111010101', 'D>>': '010010000',
                     'D<<': '010110000', 'A>>': '010000000',
                     'A<<': '010100000', 'M>>': '011000000',
                     'M<<': '011100000'}
    DESTINATION_TABLE = {'': '000', 'M': '001', 'D': '010', 'MD': '011',
                         'A': '100', 'AM': '101', 'AD': '110', 'AMD': '111'}
    JUMP_TABLE = {'': '000', 'JGT': '001', 'JEQ': '010', 'JGE': '011',
                  'JLT': '100', 'JNE': '101', 'JLE': '110', 'JMP': '111'}

    def dest(self, mnemonic):
        """
        Returns the binary code of the dest mnemonic.
        :return: Destination mnemonic (str)
        """
        return self.DESTINATION_TABLE[mnemonic]

    def comp(self, mnemonic):
        """
        Returns the binary code of the comp mnemonic
        :return: Computation mnemonic (str)
        """
        return self.COMPUTE_TABLE[mnemonic]

    def jump(self, mnemonic):
        """
        Returns the binary code of the jump mnemonic.
        :return: Jumping mnemonic (str)
        """
        return self.JUMP_TABLE[mnemonic]

if __name__ == "__main__":
    if os.path.isfile(sys.argv[1]):
        # If the input is an asm file
        if sys.argv[1].endswith('.asm'):
            asm = Assembler(sys.argv[1])
            asm.assemble()
    elif os.path.isdir(sys.argv[1]):
        # If the input is a directory go through all the asm file in it
        for item_list in os.listdir(sys.argv[1]):
            file = os.path.join(sys.argv[1], item_list)
            if file.endswith('.asm') and os.path.isfile(file):
                asm = Assembler(file)
                asm.assemble()
    else:
        print('No such file or directory')
