from project6.assembler import Code


if __name__ == "__main__":
    f = open('test.asm', 'w')
    for des in Code.DESTINATION_TABLE:
        for comp in Code.COMPUTE_TABLE:
            if not ('>>' in comp or '<<' in comp):
                for jmp in Code.JUMP_TABLE:
                    if des:
                        f.write(des + ' = ' + comp + ' ; ' + jmp + '\n')
                    else:
                        f.write(des + comp + ' ; ' + jmp + '\n')
    f.close()
