#!/bin/bash
echo ===================================================
echo running test6.sh
echo ===================================================
echo

./test6.sh

echo ===================================================
echo Done running test6.sh
echo ===================================================
echo

echo Assembling add/Add.asm
./Assembler add/Add.asm 
echo Running diff 
diff -w add/Add.hack  add/Add_ref.hack
echo

echo Assembling fill/Fill.asm
./Assembler fill/Fill.asm
echo Running diff 
diff -w fill/Fill.hack  fill/Fill_ref.hack
echo

echo Assembling max/Max.asm
./Assembler max/Max.asm
echo Running diff 
diff -w max/Max.hack max/Max_ref.hack
echo

echo Assembling max/MaxL.asm
./Assembler max/MaxL.asm
echo Running diff 
diff -w max/MaxL.hack max/MaxL_ref.hack
echo

echo Assembling mult/Mult.asm
./Assembler mult/Mult.asm
echo Running diff 
diff -w mult/Mult.hack mult/Mult_ref.hack
echo

echo Assembling pong/Pong.asm
./Assembler pong/Pong.asm
echo Running diff 
diff -w pong/Pong.hack pong/Pong_ref.hack
echo

echo Assembling ~NAND/project6/pong/PongL.asm
./Assembler ~/NAND/project6/pong/PongL.asm
echo Running diff 
diff -w pong/PongL.hack pong/PongL_ref.hack
echo

echo Assembling /cs/usr/ronenr/NAND/project6/rect/Rect.asm
./Assembler /cs/usr/ronenr/NAND/project6/rect/Rect.asm
echo Running diff 
diff -w rect/Rect.hack rect/Rect_ref.hack
echo

echo Assembling rect/RectL.asm
Assembler rect/RectL.asm
echo Running diff 
diff -w rect/RectL.hack rect/RectL_ref.hack
echo

echo Assembling ~/NAND/project6/sort/Sort.asm
./Assembler ~/NAND/project6/sort/Sort.asm
echo Running diff 
diff -w sort/Sort.hack sort/Sort_ref.hack
echo

echo Assembling t/t.asm
./Assembler t/
./Assembler t
./Assembler ~/NAND/project6/t
./Assembler ~/NAND/project6/t/
Assembler /cs/usr/ronenr/NAND/project6/t
./Assembler /cs/usr/ronenr/NAND/project6/t/
echo Running diff 
diff -w t/t.hack t/t_ref.hack
echo

echo Assembling test.asm
./Assembler test.asm
./Assembler /cs/usr/ronenr/NAND/project6/test.asm
./Assembler ~/NAND/project6/test.asm
echo Running diff 
diff -w test.hack test_ref.hack
echo

echo Assembling a none existent file
./Assembler test_stam.asm







