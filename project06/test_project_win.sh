#!/bin/bash

echo Assembling add/Add.asm
python.exe assembler.py add/Add.asm 
echo Running diff 
diff -w add/Add.hack  add/Add_ref.hack
echo

echo Assembling fill/Fill.asm
python.exe assembler.py fill/Fill.asm 
echo Running diff 
diff -w fill/Fill.hack  fill/Fill_ref.hack
echo

echo Assembling max/Max.asm
python.exe assembler.py max/Max.asm
echo Running diff 
diff -w max/Max.hack max/Max_ref.hack
echo

echo Assembling max/MaxL.asm
python.exe assembler.py max/MaxL.asm
echo Running diff 
diff -w max/MaxL.hack max/MaxL_ref.hack
echo

echo Assembling mult/Mult.asm
python.exe assembler.py mult/Mult.asm
echo Running diff 
diff -w mult/Mult.hack mult/Mult_ref.hack
echo

echo Assembling pong/Pong.asm
python.exe assembler.py pong/Pong.asm
echo Running diff 
diff -w pong/Pong.hack pong/Pong_ref.hack
echo

echo Assembling pong/PongL.asm
python.exe assembler.py pong/PongL.asm
echo Running diff 
diff -w pong/PongL.hack pong/PongL_ref.hack
echo

echo Assembling rect/Rect.asm
python.exe assembler.py rect/Rect.asm
echo Running diff 
diff -w rect/Rect.hack rect/Rect_ref.hack
echo

echo Assembling rect/RectL.asm
python.exe assembler.py rect/RectL.asm
echo Running diff 
diff -w rect/RectL.hack rect/RectL_ref.hack
echo

echo Assembling sort/Sort.asm
python.exe assembler.py sort/Sort.asm
echo Running diff 
diff -w sort/Sort.hack sort/Sort_ref.hack
echo

echo Assembling t/t.asm
python.exe assembler.py t/t.asm
echo Running diff 
diff -w t/t.hack t/t_ref.hack
echo

echo Assembling test.asm
python.exe assembler.py test.asm
echo Running diff 
diff -w test.hack test_ref.hack
echo


