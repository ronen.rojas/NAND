// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/fill/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

	@counter            // counter == 0
	M=0
(CHECKCOUNTER)
	@counter
	D=M
	@8192   
	D=D-A
	@ZEROCONUTER
	D;JGE
(READ)
	@KBD
	D=M
	@CLEARLOOP   // If Keyboard not pressed go to CLEARLOOP
	D;JEQ
	@BLACKLOOP   // If Keyboard  pressed go to BLACKLOOP
	0;JMP
(CLEARLOOP)
	@SCREEN
	D=A
	@counter
	A=D+M
	M=0
	@INCREASE
	0;JMP
(BLACKLOOP)
	@SCREEN
	D=A
	@counter
	A=D+M
	M=-1
	@INCREASE
	0;JMP
(INCREASE)
	@counter     // Increase Current counter of the pixel memory by 1
	M=M+1
	@CHECKCOUNTER
	0;JMP
(ZEROCONUTER)  // Check if the counter exceeds the memory location of the screen
	@counter
	M=0
	@READ
	0;JMP
	