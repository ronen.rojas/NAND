
// Translating MultiTest\MultiTest.vm


// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE0
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP0
	0;JMP
(TRUE0)
	@SP
	A=M-1
	M=-1
(SKIP0)

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE1
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
(SKIP1)

// push    constant     16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE2
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP2
	0;JMP
(TRUE2)
	@SP
	A=M-1
	M=-1
(SKIP2)

// push   constant        892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK3
D;JLT
@SP
A=M-1
D=M
@TRUE3
	D;JLT
@START3
0;JMP
(CHECK3)
@SP
A=M-1
D=M
@FALSE3
D;JGE
(START3)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE3
	D;JLT
	@FALSE3
	0;JMP
(TRUE3)
	@SP
	A=M-1
	M=-1
	@SKIP3
	0;JMP
(FALSE3)
	@SP
	A=M-1
	M=0
(SKIP3)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK4
D;JLT
@SP
A=M-1
D=M
@TRUE4
	D;JLT
@START4
0;JMP
(CHECK4)
@SP
A=M-1
D=M
@FALSE4
D;JGE
(START4)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE4
	D;JLT
	@FALSE4
	0;JMP
(TRUE4)
	@SP
	A=M-1
	M=-1
	@SKIP4
	0;JMP
(FALSE4)
	@SP
	A=M-1
	M=0
(SKIP4)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK5
D;JLT
@SP
A=M-1
D=M
@TRUE5
	D;JLT
@START5
0;JMP
(CHECK5)
@SP
A=M-1
D=M
@FALSE5
D;JGE
(START5)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE5
	D;JLT
	@FALSE5
	0;JMP
(TRUE5)
	@SP
	A=M-1
	M=-1
	@SKIP5
	0;JMP
(FALSE5)
	@SP
	A=M-1
	M=0
(SKIP5)

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK6
D;JGT
@SP
A=M-1
D=M
@TRUE6
	D;JGT
@START6
0;JMP
(CHECK6)
@SP
A=M-1
D=M
@FALSE6
D;JLE
(START6)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE6
	D;JGT
	@FALSE6
	0;JMP
(TRUE6)
	@SP
	A=M-1
	M=-1
	@SKIP6
	0;JMP
(FALSE6)
	@SP
	A=M-1
	M=0
(SKIP6)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK7
D;JGT
@SP
A=M-1
D=M
@TRUE7
	D;JGT
@START7
0;JMP
(CHECK7)
@SP
A=M-1
D=M
@FALSE7
D;JLE
(START7)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE7
	D;JGT
	@FALSE7
	0;JMP
(TRUE7)
	@SP
	A=M-1
	M=-1
	@SKIP7
	0;JMP
(FALSE7)
	@SP
	A=M-1
	M=0
(SKIP7)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK8
D;JGT
@SP
A=M-1
D=M
@TRUE8
	D;JGT
@START8
0;JMP
(CHECK8)
@SP
A=M-1
D=M
@FALSE8
D;JLE
(START8)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE8
	D;JGT
	@FALSE8
	0;JMP
(TRUE8)
	@SP
	A=M-1
	M=-1
	@SKIP8
	0;JMP
(FALSE8)
	@SP
	A=M-1
	M=0
(SKIP8)

// push constant 57

@57
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 31

@31
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 53

@53
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 112

@112
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// neg

@SP
A=M-1
M=-M

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push constant 82

@82
D=A
@SP
AM=M+1
A=A-1
M=D

// or

@SP
AM=M-1
D=M
A=A-1
M=D|M

// not

@SP
A=M-1
M=!M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK9
D;JLT
@SP
A=M-1
D=M
@TRUE9
	D;JLT
@START9
0;JMP
(CHECK9)
@SP
A=M-1
D=M
@FALSE9
D;JGE
(START9)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE9
	D;JLT
	@FALSE9
	0;JMP
(TRUE9)
	@SP
	A=M-1
	M=-1
	@SKIP9
	0;JMP
(FALSE9)
	@SP
	A=M-1
	M=0
(SKIP9)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK10
D;JLT
@SP
A=M-1
D=M
@TRUE10
	D;JLT
@START10
0;JMP
(CHECK10)
@SP
A=M-1
D=M
@FALSE10
D;JGE
(START10)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE10
	D;JLT
	@FALSE10
	0;JMP
(TRUE10)
	@SP
	A=M-1
	M=-1
	@SKIP10
	0;JMP
(FALSE10)
	@SP
	A=M-1
	M=0
(SKIP10)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK11
D;JLT
@SP
A=M-1
D=M
@TRUE11
	D;JLT
@START11
0;JMP
(CHECK11)
@SP
A=M-1
D=M
@FALSE11
D;JGE
(START11)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE11
	D;JLT
	@FALSE11
	0;JMP
(TRUE11)
	@SP
	A=M-1
	M=-1
	@SKIP11
	0;JMP
(FALSE11)
	@SP
	A=M-1
	M=0
(SKIP11)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK12
D;JLT
@SP
A=M-1
D=M
@TRUE12
	D;JLT
@START12
0;JMP
(CHECK12)
@SP
A=M-1
D=M
@FALSE12
D;JGE
(START12)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE12
	D;JLT
	@FALSE12
	0;JMP
(TRUE12)
	@SP
	A=M-1
	M=-1
	@SKIP12
	0;JMP
(FALSE12)
	@SP
	A=M-1
	M=0
(SKIP12)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK13
D;JLT
@SP
A=M-1
D=M
@TRUE13
	D;JLT
@START13
0;JMP
(CHECK13)
@SP
A=M-1
D=M
@FALSE13
D;JGE
(START13)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE13
	D;JLT
	@FALSE13
	0;JMP
(TRUE13)
	@SP
	A=M-1
	M=-1
	@SKIP13
	0;JMP
(FALSE13)
	@SP
	A=M-1
	M=0
(SKIP13)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK14
D;JLT
@SP
A=M-1
D=M
@TRUE14
	D;JLT
@START14
0;JMP
(CHECK14)
@SP
A=M-1
D=M
@FALSE14
D;JGE
(START14)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE14
	D;JLT
	@FALSE14
	0;JMP
(TRUE14)
	@SP
	A=M-1
	M=-1
	@SKIP14
	0;JMP
(FALSE14)
	@SP
	A=M-1
	M=0
(SKIP14)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK15
D;JLT
@SP
A=M-1
D=M
@TRUE15
	D;JLT
@START15
0;JMP
(CHECK15)
@SP
A=M-1
D=M
@FALSE15
D;JGE
(START15)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE15
	D;JLT
	@FALSE15
	0;JMP
(TRUE15)
	@SP
	A=M-1
	M=-1
	@SKIP15
	0;JMP
(FALSE15)
	@SP
	A=M-1
	M=0
(SKIP15)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK16
D;JLT
@SP
A=M-1
D=M
@TRUE16
	D;JLT
@START16
0;JMP
(CHECK16)
@SP
A=M-1
D=M
@FALSE16
D;JGE
(START16)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE16
	D;JLT
	@FALSE16
	0;JMP
(TRUE16)
	@SP
	A=M-1
	M=-1
	@SKIP16
	0;JMP
(FALSE16)
	@SP
	A=M-1
	M=0
(SKIP16)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK17
D;JLT
@SP
A=M-1
D=M
@TRUE17
	D;JLT
@START17
0;JMP
(CHECK17)
@SP
A=M-1
D=M
@FALSE17
D;JGE
(START17)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE17
	D;JLT
	@FALSE17
	0;JMP
(TRUE17)
	@SP
	A=M-1
	M=-1
	@SKIP17
	0;JMP
(FALSE17)
	@SP
	A=M-1
	M=0
(SKIP17)

// push                 constant      892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK18
D;JGT
@SP
A=M-1
D=M
@TRUE18
	D;JGT
@START18
0;JMP
(CHECK18)
@SP
A=M-1
D=M
@FALSE18
D;JLE
(START18)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE18
	D;JGT
	@FALSE18
	0;JMP
(TRUE18)
	@SP
	A=M-1
	M=-1
	@SKIP18
	0;JMP
(FALSE18)
	@SP
	A=M-1
	M=0
(SKIP18)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK19
D;JGT
@SP
A=M-1
D=M
@TRUE19
	D;JGT
@START19
0;JMP
(CHECK19)
@SP
A=M-1
D=M
@FALSE19
D;JLE
(START19)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE19
	D;JGT
	@FALSE19
	0;JMP
(TRUE19)
	@SP
	A=M-1
	M=-1
	@SKIP19
	0;JMP
(FALSE19)
	@SP
	A=M-1
	M=0
(SKIP19)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK20
D;JGT
@SP
A=M-1
D=M
@TRUE20
	D;JGT
@START20
0;JMP
(CHECK20)
@SP
A=M-1
D=M
@FALSE20
D;JLE
(START20)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE20
	D;JGT
	@FALSE20
	0;JMP
(TRUE20)
	@SP
	A=M-1
	M=-1
	@SKIP20
	0;JMP
(FALSE20)
	@SP
	A=M-1
	M=0
(SKIP20)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK21
D;JGT
@SP
A=M-1
D=M
@TRUE21
	D;JGT
@START21
0;JMP
(CHECK21)
@SP
A=M-1
D=M
@FALSE21
D;JLE
(START21)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE21
	D;JGT
	@FALSE21
	0;JMP
(TRUE21)
	@SP
	A=M-1
	M=-1
	@SKIP21
	0;JMP
(FALSE21)
	@SP
	A=M-1
	M=0
(SKIP21)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK22
D;JGT
@SP
A=M-1
D=M
@TRUE22
	D;JGT
@START22
0;JMP
(CHECK22)
@SP
A=M-1
D=M
@FALSE22
D;JLE
(START22)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE22
	D;JGT
	@FALSE22
	0;JMP
(TRUE22)
	@SP
	A=M-1
	M=-1
	@SKIP22
	0;JMP
(FALSE22)
	@SP
	A=M-1
	M=0
(SKIP22)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK23
D;JGT
@SP
A=M-1
D=M
@TRUE23
	D;JGT
@START23
0;JMP
(CHECK23)
@SP
A=M-1
D=M
@FALSE23
D;JLE
(START23)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE23
	D;JGT
	@FALSE23
	0;JMP
(TRUE23)
	@SP
	A=M-1
	M=-1
	@SKIP23
	0;JMP
(FALSE23)
	@SP
	A=M-1
	M=0
(SKIP23)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK24
D;JGT
@SP
A=M-1
D=M
@TRUE24
	D;JGT
@START24
0;JMP
(CHECK24)
@SP
A=M-1
D=M
@FALSE24
D;JLE
(START24)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE24
	D;JGT
	@FALSE24
	0;JMP
(TRUE24)
	@SP
	A=M-1
	M=-1
	@SKIP24
	0;JMP
(FALSE24)
	@SP
	A=M-1
	M=0
(SKIP24)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE25
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP25
	0;JMP
(TRUE25)
	@SP
	A=M-1
	M=-1
(SKIP25)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE26
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP26
	0;JMP
(TRUE26)
	@SP
	A=M-1
	M=-1
(SKIP26)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE27
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP27
	0;JMP
(TRUE27)
	@SP
	A=M-1
	M=-1
(SKIP27)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE28
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP28
	0;JMP
(TRUE28)
	@SP
	A=M-1
	M=-1
(SKIP28)

// push constant 1234

@1234
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 12345

@12345
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 12

@12
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 3

@3
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop local 2

@2
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 23

@23
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 2345

@2345
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 23456

@23456
D=A
@SP
AM=M+1
A=A-1
M=D

// pop argument 3

@3
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop argument 2

@2
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop argument 1

@1
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 111

@111
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 1110

@1110
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 11116

@11116
D=A
@SP
AM=M+1
A=A-1
M=D

// pop this 3

@3
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop static 3

@SP
AM=M-1
D=M
@MultiTest.3
M=D

// push constant 3333

@3333
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 33

@33
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 332

@332
D=A
@SP
AM=M+1
A=A-1
M=D

// pop that 3

@3
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop that 2

@2
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop that 1

@1
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 5000

@5000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 5010

@5010
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push pointer 1

@THAT
D=M
@SP
AM=M+1
A=A-1
M=D

// push pointer 0

@THIS
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 11

@11
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 10

@10
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 56

@56
D=A
@SP
AM=M+1
A=A-1
M=D

// pop this 3

@3
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push constant 66

@66
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 98

@98
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 324

@324
D=A
@SP
AM=M+1
A=A-1
M=D

// pop that 3

@3
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop that 2

@2
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop that 1

@1
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push static 3

@MultiTest.3
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 77

@77
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 88

@88
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 99

@99
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 3

@SP
AM=M-1
D=M
@R8
M=D

// pop temp 2

@SP
AM=M-1
D=M
@R7
M=D

// pop temp 1

@SP
AM=M-1
D=M
@R6
M=D

// push temp 3

@R8
D=M
@SP
AM=M+1
A=A-1
M=D

// push temp 1

@R6
D=M
@SP
AM=M+1
A=A-1
M=D

// push temp 2

@R7
D=M
@SP
AM=M+1
A=A-1
M=D

// Translating MultiTest\MultiTest2.vm


// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE29
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP29
	0;JMP
(TRUE29)
	@SP
	A=M-1
	M=-1
(SKIP29)

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE30
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP30
	0;JMP
(TRUE30)
	@SP
	A=M-1
	M=-1
(SKIP30)

// push    constant     16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE31
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP31
	0;JMP
(TRUE31)
	@SP
	A=M-1
	M=-1
(SKIP31)

// push   constant        892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK32
D;JLT
@SP
A=M-1
D=M
@TRUE32
	D;JLT
@START32
0;JMP
(CHECK32)
@SP
A=M-1
D=M
@FALSE32
D;JGE
(START32)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE32
	D;JLT
	@FALSE32
	0;JMP
(TRUE32)
	@SP
	A=M-1
	M=-1
	@SKIP32
	0;JMP
(FALSE32)
	@SP
	A=M-1
	M=0
(SKIP32)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK33
D;JLT
@SP
A=M-1
D=M
@TRUE33
	D;JLT
@START33
0;JMP
(CHECK33)
@SP
A=M-1
D=M
@FALSE33
D;JGE
(START33)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE33
	D;JLT
	@FALSE33
	0;JMP
(TRUE33)
	@SP
	A=M-1
	M=-1
	@SKIP33
	0;JMP
(FALSE33)
	@SP
	A=M-1
	M=0
(SKIP33)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK34
D;JLT
@SP
A=M-1
D=M
@TRUE34
	D;JLT
@START34
0;JMP
(CHECK34)
@SP
A=M-1
D=M
@FALSE34
D;JGE
(START34)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE34
	D;JLT
	@FALSE34
	0;JMP
(TRUE34)
	@SP
	A=M-1
	M=-1
	@SKIP34
	0;JMP
(FALSE34)
	@SP
	A=M-1
	M=0
(SKIP34)

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK35
D;JGT
@SP
A=M-1
D=M
@TRUE35
	D;JGT
@START35
0;JMP
(CHECK35)
@SP
A=M-1
D=M
@FALSE35
D;JLE
(START35)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE35
	D;JGT
	@FALSE35
	0;JMP
(TRUE35)
	@SP
	A=M-1
	M=-1
	@SKIP35
	0;JMP
(FALSE35)
	@SP
	A=M-1
	M=0
(SKIP35)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK36
D;JGT
@SP
A=M-1
D=M
@TRUE36
	D;JGT
@START36
0;JMP
(CHECK36)
@SP
A=M-1
D=M
@FALSE36
D;JLE
(START36)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE36
	D;JGT
	@FALSE36
	0;JMP
(TRUE36)
	@SP
	A=M-1
	M=-1
	@SKIP36
	0;JMP
(FALSE36)
	@SP
	A=M-1
	M=0
(SKIP36)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK37
D;JGT
@SP
A=M-1
D=M
@TRUE37
	D;JGT
@START37
0;JMP
(CHECK37)
@SP
A=M-1
D=M
@FALSE37
D;JLE
(START37)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE37
	D;JGT
	@FALSE37
	0;JMP
(TRUE37)
	@SP
	A=M-1
	M=-1
	@SKIP37
	0;JMP
(FALSE37)
	@SP
	A=M-1
	M=0
(SKIP37)

// push constant 57

@57
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 31

@31
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 53

@53
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 112

@112
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// neg

@SP
A=M-1
M=-M

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push constant 82

@82
D=A
@SP
AM=M+1
A=A-1
M=D

// or

@SP
AM=M-1
D=M
A=A-1
M=D|M

// not

@SP
A=M-1
M=!M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK38
D;JLT
@SP
A=M-1
D=M
@TRUE38
	D;JLT
@START38
0;JMP
(CHECK38)
@SP
A=M-1
D=M
@FALSE38
D;JGE
(START38)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE38
	D;JLT
	@FALSE38
	0;JMP
(TRUE38)
	@SP
	A=M-1
	M=-1
	@SKIP38
	0;JMP
(FALSE38)
	@SP
	A=M-1
	M=0
(SKIP38)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK39
D;JLT
@SP
A=M-1
D=M
@TRUE39
	D;JLT
@START39
0;JMP
(CHECK39)
@SP
A=M-1
D=M
@FALSE39
D;JGE
(START39)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE39
	D;JLT
	@FALSE39
	0;JMP
(TRUE39)
	@SP
	A=M-1
	M=-1
	@SKIP39
	0;JMP
(FALSE39)
	@SP
	A=M-1
	M=0
(SKIP39)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK40
D;JLT
@SP
A=M-1
D=M
@TRUE40
	D;JLT
@START40
0;JMP
(CHECK40)
@SP
A=M-1
D=M
@FALSE40
D;JGE
(START40)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE40
	D;JLT
	@FALSE40
	0;JMP
(TRUE40)
	@SP
	A=M-1
	M=-1
	@SKIP40
	0;JMP
(FALSE40)
	@SP
	A=M-1
	M=0
(SKIP40)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK41
D;JLT
@SP
A=M-1
D=M
@TRUE41
	D;JLT
@START41
0;JMP
(CHECK41)
@SP
A=M-1
D=M
@FALSE41
D;JGE
(START41)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE41
	D;JLT
	@FALSE41
	0;JMP
(TRUE41)
	@SP
	A=M-1
	M=-1
	@SKIP41
	0;JMP
(FALSE41)
	@SP
	A=M-1
	M=0
(SKIP41)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK42
D;JLT
@SP
A=M-1
D=M
@TRUE42
	D;JLT
@START42
0;JMP
(CHECK42)
@SP
A=M-1
D=M
@FALSE42
D;JGE
(START42)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE42
	D;JLT
	@FALSE42
	0;JMP
(TRUE42)
	@SP
	A=M-1
	M=-1
	@SKIP42
	0;JMP
(FALSE42)
	@SP
	A=M-1
	M=0
(SKIP42)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK43
D;JLT
@SP
A=M-1
D=M
@TRUE43
	D;JLT
@START43
0;JMP
(CHECK43)
@SP
A=M-1
D=M
@FALSE43
D;JGE
(START43)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE43
	D;JLT
	@FALSE43
	0;JMP
(TRUE43)
	@SP
	A=M-1
	M=-1
	@SKIP43
	0;JMP
(FALSE43)
	@SP
	A=M-1
	M=0
(SKIP43)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK44
D;JLT
@SP
A=M-1
D=M
@TRUE44
	D;JLT
@START44
0;JMP
(CHECK44)
@SP
A=M-1
D=M
@FALSE44
D;JGE
(START44)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE44
	D;JLT
	@FALSE44
	0;JMP
(TRUE44)
	@SP
	A=M-1
	M=-1
	@SKIP44
	0;JMP
(FALSE44)
	@SP
	A=M-1
	M=0
(SKIP44)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK45
D;JLT
@SP
A=M-1
D=M
@TRUE45
	D;JLT
@START45
0;JMP
(CHECK45)
@SP
A=M-1
D=M
@FALSE45
D;JGE
(START45)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE45
	D;JLT
	@FALSE45
	0;JMP
(TRUE45)
	@SP
	A=M-1
	M=-1
	@SKIP45
	0;JMP
(FALSE45)
	@SP
	A=M-1
	M=0
(SKIP45)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// lt

@SP
AM=M-1
D=M
@CHECK46
D;JLT
@SP
A=M-1
D=M
@TRUE46
	D;JLT
@START46
0;JMP
(CHECK46)
@SP
A=M-1
D=M
@FALSE46
D;JGE
(START46)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE46
	D;JLT
	@FALSE46
	0;JMP
(TRUE46)
	@SP
	A=M-1
	M=-1
	@SKIP46
	0;JMP
(FALSE46)
	@SP
	A=M-1
	M=0
(SKIP46)

// push                 constant      892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK47
D;JGT
@SP
A=M-1
D=M
@TRUE47
	D;JGT
@START47
0;JMP
(CHECK47)
@SP
A=M-1
D=M
@FALSE47
D;JLE
(START47)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE47
	D;JGT
	@FALSE47
	0;JMP
(TRUE47)
	@SP
	A=M-1
	M=-1
	@SKIP47
	0;JMP
(FALSE47)
	@SP
	A=M-1
	M=0
(SKIP47)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK48
D;JGT
@SP
A=M-1
D=M
@TRUE48
	D;JGT
@START48
0;JMP
(CHECK48)
@SP
A=M-1
D=M
@FALSE48
D;JLE
(START48)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE48
	D;JGT
	@FALSE48
	0;JMP
(TRUE48)
	@SP
	A=M-1
	M=-1
	@SKIP48
	0;JMP
(FALSE48)
	@SP
	A=M-1
	M=0
(SKIP48)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK49
D;JGT
@SP
A=M-1
D=M
@TRUE49
	D;JGT
@START49
0;JMP
(CHECK49)
@SP
A=M-1
D=M
@FALSE49
D;JLE
(START49)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE49
	D;JGT
	@FALSE49
	0;JMP
(TRUE49)
	@SP
	A=M-1
	M=-1
	@SKIP49
	0;JMP
(FALSE49)
	@SP
	A=M-1
	M=0
(SKIP49)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK50
D;JGT
@SP
A=M-1
D=M
@TRUE50
	D;JGT
@START50
0;JMP
(CHECK50)
@SP
A=M-1
D=M
@FALSE50
D;JLE
(START50)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE50
	D;JGT
	@FALSE50
	0;JMP
(TRUE50)
	@SP
	A=M-1
	M=-1
	@SKIP50
	0;JMP
(FALSE50)
	@SP
	A=M-1
	M=0
(SKIP50)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK51
D;JGT
@SP
A=M-1
D=M
@TRUE51
	D;JGT
@START51
0;JMP
(CHECK51)
@SP
A=M-1
D=M
@FALSE51
D;JLE
(START51)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE51
	D;JGT
	@FALSE51
	0;JMP
(TRUE51)
	@SP
	A=M-1
	M=-1
	@SKIP51
	0;JMP
(FALSE51)
	@SP
	A=M-1
	M=0
(SKIP51)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK52
D;JGT
@SP
A=M-1
D=M
@TRUE52
	D;JGT
@START52
0;JMP
(CHECK52)
@SP
A=M-1
D=M
@FALSE52
D;JLE
(START52)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE52
	D;JGT
	@FALSE52
	0;JMP
(TRUE52)
	@SP
	A=M-1
	M=-1
	@SKIP52
	0;JMP
(FALSE52)
	@SP
	A=M-1
	M=0
(SKIP52)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK53
D;JGT
@SP
A=M-1
D=M
@TRUE53
	D;JGT
@START53
0;JMP
(CHECK53)
@SP
A=M-1
D=M
@FALSE53
D;JLE
(START53)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE53
	D;JGT
	@FALSE53
	0;JMP
(TRUE53)
	@SP
	A=M-1
	M=-1
	@SKIP53
	0;JMP
(FALSE53)
	@SP
	A=M-1
	M=0
(SKIP53)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE54
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP54
	0;JMP
(TRUE54)
	@SP
	A=M-1
	M=-1
(SKIP54)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE55
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP55
	0;JMP
(TRUE55)
	@SP
	A=M-1
	M=-1
(SKIP55)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE56
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP56
	0;JMP
(TRUE56)
	@SP
	A=M-1
	M=-1
(SKIP56)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE57
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP57
	0;JMP
(TRUE57)
	@SP
	A=M-1
	M=-1
(SKIP57)

// push constant 1234

@1234
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 12345

@12345
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 12

@12
D=A
@SP
AM=M+1
A=A-1
M=D

// pop local 3

@3
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop local 2

@2
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop local 1

@1
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 3

@3
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 1

@1
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push local 2

@2
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 23

@23
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 2345

@2345
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 23456

@23456
D=A
@SP
AM=M+1
A=A-1
M=D

// pop argument 3

@3
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop argument 2

@2
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop argument 1

@1
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 3

@3
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 1

@1
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push argument 2

@2
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 5000

@5000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 5010

@5010
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push pointer 1

@THAT
D=M
@SP
AM=M+1
A=A-1
M=D

// push pointer 0

@THIS
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 11

@11
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 10

@10
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 56

@56
D=A
@SP
AM=M+1
A=A-1
M=D

// pop this 3

@3
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop this 2

@2
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop this 1

@1
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 3

@3
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push this 1

@1
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push this 2

@2
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 66

@66
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 98

@98
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 324

@324
D=A
@SP
AM=M+1
A=A-1
M=D

// pop that 3

@3
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop that 2

@2
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// pop that 1

@1
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push that 3

@3
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push that 1

@1
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push that 2

@2
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 77

@77
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 88

@88
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 99

@99
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 3

@SP
AM=M-1
D=M
@R8
M=D

// pop temp 2

@SP
AM=M-1
D=M
@R7
M=D

// pop temp 1

@SP
AM=M-1
D=M
@R6
M=D

// push temp 3

@R8
D=M
@SP
AM=M+1
A=A-1
M=D

// push temp 1

@R6
D=M
@SP
AM=M+1
A=A-1
M=D

// push temp 2

@R7
D=M
@SP
AM=M+1
A=A-1
M=D

// pop static 3

@SP
AM=M-1
D=M
@MultiTest2.3
M=D
