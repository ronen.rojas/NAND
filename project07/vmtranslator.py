import os
import sys
import re


class Parser:
    """
    The Parser class Handles the parsing of a single .vm file, and encapsulates
    access to the input code. It reads VM commands, parses them, and provides
    convenient access to their components. In addition, it removes all white
    space and comments.
    """
    COMMENT_PREFIX = "//"
    C_ARITHMETIC = 0
    C_PUSH = 1
    C_POP = 2
    C_LABEL = 3
    C_GOTO = 4
    C_IF = 5
    C_FUNCTION = 6
    C_RETURN = 7
    C_CALL = 8
    COMMAND_DICT = {'add': C_ARITHMETIC, 'sub': C_ARITHMETIC,
                    'neg': C_ARITHMETIC, 'eq': C_ARITHMETIC,
                    'gt': C_ARITHMETIC, 'lt': C_ARITHMETIC,
                    'and': C_ARITHMETIC, 'or': C_ARITHMETIC,
                    'not': C_ARITHMETIC, 'push': C_PUSH, 'pop': C_POP,
                    'label': C_LABEL, 'goto': C_GOTO, 'if-goto': C_IF,
                    'function': C_FUNCTION, 'return': C_RETURN, 'call': C_CALL}
    COMMENT_RE = re.compile('(//.*\n)|(/\*(.|\n)*?\*/)')

    def __init__(self, input_file):
        """
        The constructor opens the input file/stream and gets ready to parse it.
        :param input_file: Name of the vm file to translate (str)
        """
        # Opening the input file getting rid of comments and blank lines
        self.lines_list = [line.strip() for line in
                           self._clean_comment(input_file) if line.strip()]
        self.current_index = -1

    def __getitem__(self, idx):
        return self.lines_list[idx].replace('\t', ' ')

    def _clean_comment(self, vm_file):
        """
        Cleaning block comment and end line comments from the vm code.
        :param vm_file:
        :return: No return value
        """
        return self.COMMENT_RE.sub('\n', open(vm_file).read()).split('\n')

    def get_current_line(self):
        """
        Getting the current line of the parser.
        :return: Current line (str)
        """
        return self[self.current_index]

    def reset(self):
        """
        Resetting the state of parser the beginning of the file.
        :return: No return value
        """
        self.current_index = -1

    def has_more_commands(self):
        """
         Are there more commands in the input?
        :return: Boolean - True has more commands False otherwise
        """
        return self.current_index + 1 < len(self.lines_list)

    def advance(self):
        """
        Reads the next command from the input and makes it the current command.
        Should be  called only if has_more_commands() is true.
        Initially there is no current command.
        :return: No return value
        """
        self.current_index += 1

    def command_type(self):
        """
        Returns the type of the current VM command. C_ARITHMETIC is returned
        for all the arithmetic commands.
        :return: Virtual machine command type (int)
        """
        return self.COMMAND_DICT[self[self.current_index].partition(' ')[0]]

    def arg1(self):
        """
        Returns the first argument of the current command. In the case of
        C_ARITHMETIC, the command itself (add, sub, etc.) is returned. Should
        not be called if the current command is C_RETURN.
        :return: 1st argument of virtual machine (str)
        """
        if self.command_type() == self.C_ARITHMETIC:
            return self[self.current_index].partition(' ')[0].strip()
        else:
            out_str = self[self.current_index].partition(' ')[-1].strip()
            return out_str.partition(' ')[0].strip()

    def arg2(self):
        """
        Returns the second argument of the current command. Should be called
        only if the current command is C_PUSH, C_POP, C_FUNCTION, or C_CALL
        :return: 2nd argument of virtual machine (str)
        """
        out_str = self[self.current_index].partition(' ')[-1].strip()
        return out_str.partition(' ')[-1].strip()


class CodeWriter:
    """
    The CodeWriter class  Translates VM commands into Hack assembly code.
    """
    C_BINARY = 0
    C_UNARY = 1
    TYPE_IDX = 0
    CODE_IDX = 1
    COMMAND_TYPE = {'add': (C_BINARY, 'M=D+M\n'),
                    'sub': (C_BINARY, 'M=M-D\n'),
                    'neg': (C_UNARY, 'M=-M\n'),
                    'eq': (C_BINARY, 'D=M-D\n'),
                    'gt': (C_BINARY, ''),
                    'lt': (C_BINARY, ''),
                    'and': (C_BINARY, 'M=D&M\n'),
                    'or': (C_BINARY, 'M=D|M\n'),
                    'not': (C_UNARY, 'M=!M\n')}
    MEMORY_NAME = {'local': 'LCL', 'argument': 'ARG', 'this': 'THIS',
                   'that': 'THAT'}
    POINTER_IDX = {'0': 'THIS', '1': 'THAT'}
    LABEL_SEP = '$'

    # Common assembly code:
    BOOTSTRAP = '@256\nD=A\n@SP\nM=D\n'
    PUSH_D_TO_STACK = '@SP\nAM=M+1\nA=A-1\nM=D\n'
    PUSH_ZERO_TO_STACK = '@SP\nAM=M+1\nA=A-1\nM=0\n'
    POP_STACK_TO_D = '@SP\nAM=M-1\nD=M\n'
    L_INSTRUCTION = '({0})\n'
    A_INSTRUCTION = '@{0}\n'
    JUMP = '0;JMP\n'
    JUMP_LOGIC = 'D;{0}\n'
    SET_STACK = '@SP\n\tA=M-1\n\tM={0}\n'
    TRUE = -1
    FALSE = 0
    DECREASE_A = 'A=A-1\n'
    STACK_TO_D = '@SP\nA=M-1\nD=M\n'

    def __init__(self, output_file, init_flag):
        """
        Opens the output file/stream and gets ready to write into it.
        :param output_file: Assembly (.asm) file name (str)
        :param init_flag: A flag indicating whether it is needed to write
                          the bootstrap code (True) or not (False) (Boolean)
        """
        self.out_object = open(output_file, 'w')
        self.parser = None
        self.instruction_idx = 0
        self.file_name = None
        self.function_name = None
        # Bootstrap code @ ROM 0x0000:
        if init_flag:
            self.write_init()

    def set_file_name(self, file_name):
        """
        Informs the code writer that the translation of a new VM file is
        started.
        :return: No return value
        """
        self.file_name = os.path.splitext(os.path.basename(file_name))[0]
        self.parser = Parser(file_name)

    def translate_file(self):
        """
        Translates the vm file command by command.
        :return: No return value.
        """
        while self.parser.has_more_commands():
            self.parser.advance()
            self.write_comment(self.parser.get_current_line())
            command_type = self.parser.command_type()
            pre_label = self.function_name + self.LABEL_SEP if \
                self.function_name else ''
            if command_type == self.parser.C_ARITHMETIC:
                self.write_arithmetic(self.parser.arg1())
            elif command_type == self.parser.C_PUSH:
                self.write_push(self.parser.arg1(), self.parser.arg2())
            elif command_type == self.parser.C_POP:
                self.write_pop(self.parser.arg1(), self.parser.arg2())
            elif command_type == self.parser.C_LABEL:
                self.write_label(pre_label + self.parser.arg1())
            elif command_type == self.parser.C_GOTO:
                self.write_go_to(pre_label + self.parser.arg1())
            elif command_type == self.parser.C_IF:
                self.write_if(pre_label + self.parser.arg1())
            elif command_type == self.parser.C_CALL:
                self.write_call(self.parser.arg1(), self.parser.arg2())
            elif command_type == self.parser.C_FUNCTION:
                self.write_function(self.parser.arg1(), self.parser.arg2())
            elif command_type == self.parser.C_RETURN:
                self.write_return()

    def write_init(self):
        """
        Writes the assembly code that effects the VM initialization (also
        called bootstrap code). This code should be placed in the ROM
        beginning in address 0x0000.
        :return: No return value.
        """
        self.out_object.write(self.BOOTSTRAP)
        self.write_call('Sys.init', '0')

    def write_label(self, label):
        """
        Writes the assembly code that is the translation of the given label
        command.
        :param label: The label input (str)
        :return: No return value.
        """
        self.out_object.write(self.L_INSTRUCTION.format(label))

    def write_go_to(self, label):
        """
        Writes the assembly code that is the translation of the given goto
        command.
        :param label: The label input (str)
        :return: No return value.
        """
        self._write_a_instruction(label)
        self.out_object.write(self.JUMP)

    def write_if(self, label):
        """
        Writes the assembly code that is the translation of the given
        if-goto command.
        :param label: The label input (str)
        :return: No return value.
        """
        self._write_pop_stack_to_d()
        self._write_a_instruction(label)
        self.out_object.write(self.JUMP_LOGIC.format('JNE'))

    def write_call(self, func_name, num_arg):
        """
        Writes the assembly code that is the translation of the given Call
        command.
        :param func_name: The name of the function (str)
        :param num_arg:  Number of arguments of the function (str)
        :return: No return value.
        """
        # push return-address: (Using the label declared below)
        return_label = func_name + str(self.instruction_idx)
        self._assign_a_to_d(return_label)
        self._write_push_d_to_stack()
        # push LCL: Save LCL of the calling function
        self._assign_m_to_d('LCL')
        self._write_push_d_to_stack()
        # push ARG: Save ARG of the calling function
        self._assign_m_to_d('ARG')
        self._write_push_d_to_stack()
        # push THIS: Save THIS of the calling function
        self._assign_m_to_d('THIS')
        self._write_push_d_to_stack()
        # push THAT: Save THAT of the calling function
        self._assign_m_to_d('THAT')
        self._write_push_d_to_stack()
        # ARG = SP - n - 5 : Reposition ARG (n == number of args.)
        self._assign_a_to_d(num_arg)
        self._assign_m_to_d('SP', offset='D')
        self._decrease_d(5)
        self._assign_d_to_m('ARG')
        # LCL =SP : Reposition LCL
        self._assign_m_to_d('SP')
        self._assign_d_to_m('LCL')
        # goto f : Transfer control
        self.write_go_to(func_name)
        # (return-address) - Declare a label for the return-address
        self.write_label(return_label)
        self.instruction_idx += 1

    def write_function(self, func_name, num_local):
        """
        Writes the assembly code that is the translation of the given Function
        command.
        :param func_name: The name of the function (str)
        :param num_local: Number of local variables of the function (str)
        :return: No return value.
        """
        # (f) : Declare a label for the function entry
        self.write_label(func_name)
        #  Initialize all local variables to 0
        for _ in range(int(num_local)):
            self.out_object.write(self.PUSH_ZERO_TO_STACK)
        # For the use of inner labels of the function
        self.function_name = func_name

    def write_return(self):
        """
        Writes the assembly code that is the translation of the given Return
        command.
        :return: No return value.
        """
        # FRAME = LCL : FRAME is a temporary variable
        # FRAME = R14
        self._assign_m_to_d('LCL')
        self._assign_d_to_m('R14')
        # RET = *(FRAME-5) : Put the return-address in a temp. var
        # RET = R15
        self._assign_a_to_d('5')
        self._assign_pointer_to_d('R14', offset='D')
        self._assign_d_to_m('R15')
        # *ARG = pop() : Reposition the return value for the caller
        self._assign_pointer_to_d('SP', offset='1')
        self._assign_d_to_pointer('ARG')
        # SP = ARG+1 : Restore SP of the caller
        self._assign_m_to_d('ARG')
        self.out_object.write('@SP\nM=D+1\n')
        # THAT = *(FRAME-1) : Restore THAT of the caller
        self._assign_pointer_to_d('R14', offset='1')
        self._assign_d_to_m('THAT')
        # THIS = *(FRAME-2) : Restore THIS  of the caller
        self._assign_a_to_d('2')
        self._assign_pointer_to_d('R14', offset='D')
        self._assign_d_to_m('THIS')
        # ARG = *(FRAME-3) : Restore ARG of the caller
        self._assign_a_to_d('3')
        self._assign_pointer_to_d('R14', offset='D')
        self._assign_d_to_m('ARG')
        # LCL = *(FRAME-4) : Restore LCL of the caller
        self._assign_a_to_d('4')
        self._assign_pointer_to_d('R14', offset='D')
        self._assign_d_to_m('LCL')
        # goto RET : // Goto return address (in the caller's code)
        self.out_object.write('@R15\nA=M\n')
        self.out_object.write(self.JUMP)

    def write_arithmetic(self, command):
        """
        Writes the assembly code that is the translation of the given
        arithmetic command.
        :param command: The arithmetic command (str)
        :return: No return value.
        """
        if self.COMMAND_TYPE[command][self.TYPE_IDX] == self.C_BINARY:
            self.write_arithmetic_binary(command)
        else:
            self.write_arithmetic_unary(command)

    def write_arithmetic_binary(self, command):
        """
        Writes the assembly code that is the translation of the given binary
        arithmetic command.
        :param command: The arithmetic binary command (str)
        :return: No return value.
        """
        self._write_pop_stack_to_d()
        if command == 'eq':
            self.out_object.write(self.DECREASE_A)
            self.out_object.write(self.COMMAND_TYPE[command][self.CODE_IDX])
            self._write_arithmetic_eq()
        elif command in ('lt', 'gt'):
            self._write_arithmetic_inequality(command)
        else:
            self.out_object.write(self.DECREASE_A)
            self.out_object.write(self.COMMAND_TYPE[command][self.CODE_IDX])

    def _write_arithmetic_eq(self):
        """
        Writes the assembly code that is the translation of the equal
        arithmetic command.
        :return: No return value.
        """
        label_str_true = 'TRUE' + str(self.instruction_idx)
        label_str_skip = 'SKIP' + str(self.instruction_idx)
        self._write_a_instruction(label_str_true)
        self.out_object.write('\t' + self.JUMP_LOGIC.format('JEQ') + '\t' +
                              self.SET_STACK.format(self.FALSE))
        self._write_a_instruction(label_str_skip, prefix='\t')
        self.out_object.write('\t' + self.JUMP)
        self.write_label(label_str_true)
        self.out_object.write('\t' + self.SET_STACK.format(self.TRUE))
        self.write_label(label_str_skip)
        self.instruction_idx += 1

    def _write_arithmetic_inequality(self, command):
        """
        Writes the assembly code that is the translation of the less than /
        greater than arithmetic command.
        :return: No return value.
        """
        jmp_com = 'J' + command.upper()
        label_str_start = 'START' + str(self.instruction_idx)
        label_str_false = 'FALSE' + str(self.instruction_idx)
        label_str_true = 'TRUE' + str(self.instruction_idx)
        label_str_skip = 'SKIP' + str(self.instruction_idx)
        label_str_check = 'CHECK' + str(self.instruction_idx)

        # Special treatment for overflow
        self._write_a_instruction(label_str_check)
        self.out_object.write(self.JUMP_LOGIC.format(jmp_com))
        self.out_object.write(self.STACK_TO_D)
        self._write_a_instruction(label_str_true)
        self.out_object.write('\t' + self.JUMP_LOGIC.format(jmp_com))
        self._write_a_instruction(label_str_start)
        self.out_object.write(self.JUMP)
        self.write_label(label_str_check)
        self.out_object.write(self.STACK_TO_D)
        self._write_a_instruction(label_str_false)
        jmp_com_temp = 'JGE' if command == 'lt' else 'JLE'
        self.out_object.write(self.JUMP_LOGIC.format(jmp_com_temp))

        # Start regular treatment
        self.write_label(label_str_start)

        self.out_object.write(self.STACK_TO_D)
        self.out_object.write('\t@SP\n\tA=M\n\tD=D-M\n')
        # D = x - y
        self._write_a_instruction(label_str_true, prefix='\t')
        self.out_object.write('\t' + self.JUMP_LOGIC.format(jmp_com))
        self._write_a_instruction(label_str_false, prefix='\t')
        self.out_object.write('\t' + self.JUMP)
        self.write_label(label_str_true)
        self.out_object.write('\t' + self.SET_STACK.format(self.TRUE))
        self._write_a_instruction(label_str_skip, prefix='\t')
        self.out_object.write('\t' + self.JUMP)
        self.write_label(label_str_false)
        self.out_object.write('\t' + self.SET_STACK.format(self.FALSE))
        self.write_label(label_str_skip)
        self.instruction_idx += 1

    def write_arithmetic_unary(self, command):
        """
        Writes the assembly code that is the translation of the given
        unary arithmetic command
        :param command: The arithmetic unary  command (str)
        :return: No return value.
        """
        self.out_object.write('@SP\nA=M-1\n')
        self.out_object.write(self.COMMAND_TYPE[command][self.CODE_IDX])

    def write_push(self, segment, index):
        """
        Writes the assembly code that is the translation of the push command.
        :param segment: The memory segment (str)
        :param index: The location in the memory segment (str)
        :return: No return value.
        """
        if segment == 'constant':
            self._assign_a_to_d(index)
        elif segment in self.MEMORY_NAME:
            self._assign_a_to_d(index)
            self._write_a_instruction(self.MEMORY_NAME[segment])
            self.out_object.write('A=D+M\nD=M\n')
        elif segment == 'temp':
            self._assign_m_to_d('R' + str(5 + int(index)))
        elif segment == 'pointer':
            self._assign_m_to_d(self.POINTER_IDX[index])
        elif segment == 'static':
            self._assign_m_to_d(self.file_name + '.' + index)
        self._write_push_d_to_stack()

    def write_pop(self, segment, index):
        """
         Writes the assembly code that is the translation of the pop command.
         :param segment: The memory segment (str)
         :param index: The location in the memory segment (str)
         :return: No return value.
         """
        if segment in self.MEMORY_NAME:
            self._assign_a_to_d(index)
            self._write_a_instruction(self.MEMORY_NAME[segment])
            self.out_object.write('D=D+M\n')
            self._assign_d_to_m('R13')
            self._write_pop_stack_to_d()
            self._assign_d_to_pointer('R13')
        elif segment == 'temp':
            self._write_pop_stack_to_d()
            self._assign_d_to_m('R' + str(5 + int(index)))
        elif segment == 'pointer':
            self._write_pop_stack_to_d()
            self._assign_d_to_m(self.POINTER_IDX[index])
        elif segment == 'static':
            self._write_pop_stack_to_d()
            self._assign_d_to_m(self.file_name + '.' + index)

    def write_comment(self, comment_str):
        """
        Write a comment in the assembly file for readability of the code.
        :param comment_str: Comment string (str)
        :return:
        """
        self.out_object.write('\n// ' + comment_str + 2 * '\n')

    def close(self):
        """
        Closes the output file.
        :return: No return value.
        """
        self.out_object.close()

    def translate_vm_file(self, vm_file):
        """
        Encapsulates all of the translation of the vm file.
        :param vm_file: The virtual machine code file.
        :return: No return value.
        """
        self.write_comment('Translating ' + vm_file)
        self.set_file_name(vm_file)
        self.translate_file()

    def _write_push_d_to_stack(self):
        """
        Writes the assembly code for pushing the D register to the stack.
        :return: No return value.
        """

        self.out_object.write(self.PUSH_D_TO_STACK)

    def _write_pop_stack_to_d(self):
        """
        Writes the assembly code for popping the stack to D register.
        :return: No return value.
        """
        self.out_object.write(self.POP_STACK_TO_D)

    def _write_a_instruction(self, a_arg, prefix=''):
        """
        Writes the assembly code for A instruction.
        :param a_arg: The A register argument (str)
        :return: No return value
        """
        self.out_object.write(prefix + self.A_INSTRUCTION.format(a_arg))

    def _assign_a_to_d(self, a_arg):
        """
        Writes the assembly code for assigning the A register to the D register
        :param a_arg: The A register argument (str)
        :return: No return value
        """
        self._write_a_instruction(a_arg)
        self.out_object.write('D=A\n')

    def _assign_m_to_d(self, a_arg, offset=None):
        """
        Writes the assembly code for assigning the M register to the D register
        :param a_arg: The A register argument (str)
        :return: No return value
        """
        self._write_a_instruction(a_arg)
        if offset:
            if offset == '1':
                self.out_object.write('D=M-1\n')
            else:
                self.out_object.write('D=M-D\n')
        else:
            self.out_object.write('D=M\n')

    def _assign_d_to_m(self, a_arg):
        """
        Writes the assembly code for assigning the D register to the M register
        :param a_arg: The A register argument (str)
        :return: No return value
        """
        self._write_a_instruction(a_arg)
        self.out_object.write('M=D\n')

    def _assign_d_to_pointer(self, pointer_arg, offset=None):
        """
        Writes the assembly code for assigning the D register to the pointer
        value
        :param pointer_arg: The  pointer argument (str)
        :return: No return value
        """
        self._write_a_instruction(pointer_arg)
        if offset:
            if offset == '1':
                self.out_object.write('A=M-1\n')
            else:
                self.out_object.write('A=M-D\n')
        else:
            self.out_object.write('A=M\n')
        self.out_object.write('M=D\n')

    def _assign_pointer_to_d(self, pointer_arg, offset=None):
        """
        Writes the assembly code for assigning pointer value to the D
        register to the pointer
        :param pointer_arg: The  pointer argument (str)
        :return: No return value
        """
        self._write_a_instruction(pointer_arg)
        if offset:
            if offset == '1':
                self.out_object.write('A=M-1\n')
            else:
                self.out_object.write('A=M-D\n')
        else:
            self.out_object.write('A=M\n')
        self.out_object.write('D=M\n')

    def _decrease_d(self, num):
        """
        Writes the assembly code for decreasing the value of D in an integer
        value
        :param num: The integer value to be decreased
        :return: No return value
        """
        self._write_a_instruction(str(num))
        self.out_object.write('D=D-A\n')


if __name__ == "__main__":
    # Flag from user for bootstrap code
    bootstrap_flag = sys.argv[1] == 'bootstrap'
    # Input string from user
    input_str = sys.argv[2]
    if os.path.isfile(input_str):
        # Translation of the single file
        assembly_file = input_str.replace('.vm', '.asm')
        code_writer = CodeWriter(assembly_file, bootstrap_flag)
        code_writer.translate_vm_file(input_str)
        code_writer.close()
    elif os.path.isdir(input_str):
        # Translation of all the vm files in the directory to one bundled asm
        basename = os.path.basename(input_str.rstrip(os.path.sep)) + '.asm'
        assembly_file = os.path.join(input_str, basename)
        code_writer = CodeWriter(assembly_file, bootstrap_flag)
        for item in os.listdir(input_str):
            file = os.path.join(input_str, item)
            if file.endswith('.vm') and os.path.isfile(file):
                code_writer.translate_vm_file(file)
        code_writer.close()
    else:
        print("no such file or directory")
