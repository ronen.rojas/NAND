
// Translating VMTest\VMTest.vm


// push constant 17000

@17000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17001

@17001
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK0
D;JLT
@SP
A=M-1
D=M
@TRUE0
	D;JLT
@START0
0;JMP
(CHECK0)
@SP
A=M-1
D=M
@FALSE0
D;JGE
(START0)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE0
	D;JLT
	@FALSE0
	0;JMP
(TRUE0)
	@SP
	A=M-1
	M=-1
	@SKIP0
	0;JMP
(FALSE0)
	@SP
	A=M-1
	M=0
(SKIP0)

// push constant 38

@38
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 12

@12
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 49

@49
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK1
D;JGT
@SP
A=M-1
D=M
@TRUE1
	D;JGT
@START1
0;JMP
(CHECK1)
@SP
A=M-1
D=M
@FALSE1
D;JLE
(START1)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE1
	D;JGT
	@FALSE1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
	@SKIP1
	0;JMP
(FALSE1)
	@SP
	A=M-1
	M=0
(SKIP1)

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE2
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP2
	0;JMP
(TRUE2)
	@SP
	A=M-1
	M=-1
(SKIP2)

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE3
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP3
	0;JMP
(TRUE3)
	@SP
	A=M-1
	M=-1
(SKIP3)

// push constant 4

@4
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 2

@2
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// gt

@SP
AM=M-1
D=M
@CHECK4
D;JGT
@SP
A=M-1
D=M
@TRUE4
	D;JGT
@START4
0;JMP
(CHECK4)
@SP
A=M-1
D=M
@FALSE4
D;JLE
(START4)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE4
	D;JGT
	@FALSE4
	0;JMP
(TRUE4)
	@SP
	A=M-1
	M=-1
	@SKIP4
	0;JMP
(FALSE4)
	@SP
	A=M-1
	M=0
(SKIP4)

// push constant 5

@5
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK5
D;JLT
@SP
A=M-1
D=M
@TRUE5
	D;JLT
@START5
0;JMP
(CHECK5)
@SP
A=M-1
D=M
@FALSE5
D;JGE
(START5)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE5
	D;JLT
	@FALSE5
	0;JMP
(TRUE5)
	@SP
	A=M-1
	M=-1
	@SKIP5
	0;JMP
(FALSE5)
	@SP
	A=M-1
	M=0
(SKIP5)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// not

@SP
A=M-1
M=!M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE6
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP6
	0;JMP
(TRUE6)
	@SP
	A=M-1
	M=-1
(SKIP6)

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 1

@1
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// or

@SP
AM=M-1
D=M
A=A-1
M=D|M

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// pop argument 0

@0
D=A
@ARG
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push argument 0

@0
D=A
@ARG
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop local 0

@0
D=A
@LCL
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push local 0

@0
D=A
@LCL
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// pop static 2

@SP
AM=M-1
D=M
@VMTest.2
M=D

// push static 2

@VMTest.2
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 4000

@4000
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 0

@SP
AM=M-1
D=M
@THIS
M=D

// push constant 18

@18
D=A
@SP
AM=M+1
A=A-1
M=D

// pop this 0

@0
D=A
@THIS
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push this 0

@0
D=A
@THIS
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 5000

@5000
D=A
@SP
AM=M+1
A=A-1
M=D

// pop pointer 1

@SP
AM=M-1
D=M
@THAT
M=D

// push constant 21

@21
D=A
@SP
AM=M+1
A=A-1
M=D

// pop that 0

@0
D=A
@THAT
D=D+M
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D

// push that 0

@0
D=A
@THAT
A=D+M
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 22

@22
D=A
@SP
AM=M+1
A=A-1
M=D

// pop temp 3

@SP
AM=M-1
D=M
@R8
M=D

// push temp 3

@R8
D=M
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// gt

@SP
AM=M-1
D=M
@CHECK7
D;JGT
@SP
A=M-1
D=M
@TRUE7
	D;JGT
@START7
0;JMP
(CHECK7)
@SP
A=M-1
D=M
@FALSE7
D;JLE
(START7)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE7
	D;JGT
	@FALSE7
	0;JMP
(TRUE7)
	@SP
	A=M-1
	M=-1
	@SKIP7
	0;JMP
(FALSE7)
	@SP
	A=M-1
	M=0
(SKIP7)

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 30000

@30000
D=A
@SP
AM=M+1
A=A-1
M=D

// neg

@SP
A=M-1
M=-M

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// push constant 0

@0
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK8
D;JGT
@SP
A=M-1
D=M
@TRUE8
	D;JGT
@START8
0;JMP
(CHECK8)
@SP
A=M-1
D=M
@FALSE8
D;JLE
(START8)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE8
	D;JGT
	@FALSE8
	0;JMP
(TRUE8)
	@SP
	A=M-1
	M=-1
	@SKIP8
	0;JMP
(FALSE8)
	@SP
	A=M-1
	M=0
(SKIP8)
