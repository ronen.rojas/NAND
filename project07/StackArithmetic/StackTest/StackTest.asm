
// Translating StackArithmetic/StackTest/StackTest.vm


// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE0
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP0
	0;JMP
(TRUE0)
	@SP
	A=M-1
	M=-1
(SKIP0)

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE1
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP1
	0;JMP
(TRUE1)
	@SP
	A=M-1
	M=-1
(SKIP1)

// push constant 16

@16
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 17

@17
D=A
@SP
AM=M+1
A=A-1
M=D

// eq

@SP
AM=M-1
D=M
A=A-1
D=M-D
@TRUE2
	D;JEQ
	@SP
	A=M-1
	M=0
	@SKIP2
	0;JMP
(TRUE2)
	@SP
	A=M-1
	M=-1
(SKIP2)

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK3
D;JLT
@SP
A=M-1
D=M
@TRUE3
	D;JLT
@START3
0;JMP
(CHECK3)
@SP
A=M-1
D=M
@FALSE3
D;JGE
(START3)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE3
	D;JLT
	@FALSE3
	0;JMP
(TRUE3)
	@SP
	A=M-1
	M=-1
	@SKIP3
	0;JMP
(FALSE3)
	@SP
	A=M-1
	M=0
(SKIP3)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 892

@892
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK4
D;JLT
@SP
A=M-1
D=M
@TRUE4
	D;JLT
@START4
0;JMP
(CHECK4)
@SP
A=M-1
D=M
@FALSE4
D;JGE
(START4)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE4
	D;JLT
	@FALSE4
	0;JMP
(TRUE4)
	@SP
	A=M-1
	M=-1
	@SKIP4
	0;JMP
(FALSE4)
	@SP
	A=M-1
	M=0
(SKIP4)

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 891

@891
D=A
@SP
AM=M+1
A=A-1
M=D

// lt

@SP
AM=M-1
D=M
@CHECK5
D;JLT
@SP
A=M-1
D=M
@TRUE5
	D;JLT
@START5
0;JMP
(CHECK5)
@SP
A=M-1
D=M
@FALSE5
D;JGE
(START5)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE5
	D;JLT
	@FALSE5
	0;JMP
(TRUE5)
	@SP
	A=M-1
	M=-1
	@SKIP5
	0;JMP
(FALSE5)
	@SP
	A=M-1
	M=0
(SKIP5)

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK6
D;JGT
@SP
A=M-1
D=M
@TRUE6
	D;JGT
@START6
0;JMP
(CHECK6)
@SP
A=M-1
D=M
@FALSE6
D;JLE
(START6)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE6
	D;JGT
	@FALSE6
	0;JMP
(TRUE6)
	@SP
	A=M-1
	M=-1
	@SKIP6
	0;JMP
(FALSE6)
	@SP
	A=M-1
	M=0
(SKIP6)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32767

@32767
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK7
D;JGT
@SP
A=M-1
D=M
@TRUE7
	D;JGT
@START7
0;JMP
(CHECK7)
@SP
A=M-1
D=M
@FALSE7
D;JLE
(START7)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE7
	D;JGT
	@FALSE7
	0;JMP
(TRUE7)
	@SP
	A=M-1
	M=-1
	@SKIP7
	0;JMP
(FALSE7)
	@SP
	A=M-1
	M=0
(SKIP7)

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 32766

@32766
D=A
@SP
AM=M+1
A=A-1
M=D

// gt

@SP
AM=M-1
D=M
@CHECK8
D;JGT
@SP
A=M-1
D=M
@TRUE8
	D;JGT
@START8
0;JMP
(CHECK8)
@SP
A=M-1
D=M
@FALSE8
D;JLE
(START8)
@SP
A=M-1
D=M
	@SP
	A=M
	D=D-M
	@TRUE8
	D;JGT
	@FALSE8
	0;JMP
(TRUE8)
	@SP
	A=M-1
	M=-1
	@SKIP8
	0;JMP
(FALSE8)
	@SP
	A=M-1
	M=0
(SKIP8)

// push constant 57

@57
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 31

@31
D=A
@SP
AM=M+1
A=A-1
M=D

// push constant 53

@53
D=A
@SP
AM=M+1
A=A-1
M=D

// add

@SP
AM=M-1
D=M
A=A-1
M=D+M

// push constant 112

@112
D=A
@SP
AM=M+1
A=A-1
M=D

// sub

@SP
AM=M-1
D=M
A=A-1
M=M-D

// neg

@SP
A=M-1
M=-M

// and

@SP
AM=M-1
D=M
A=A-1
M=D&M

// push constant 82

@82
D=A
@SP
AM=M+1
A=A-1
M=D

// or

@SP
AM=M-1
D=M
A=A-1
M=D|M

// not

@SP
A=M-1
M=!M
