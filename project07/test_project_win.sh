#!/bin/bash

echo Translating  StackArithmetic/SimpleAdd/SimpleAdd.vm
python.exe vmtranslator.py no_bootstrap StackArithmetic/SimpleAdd/SimpleAdd.vm
CPUEmulator.bat StackArithmetic/SimpleAdd/SimpleAdd.tst
echo

echo Translating  StackArithmetic/StackTest/StackTest.vm
python.exe vmtranslator.py no_bootstrap StackArithmetic/StackTest/StackTest.vm
CPUEmulator.bat StackArithmetic/StackTest/StackTest.tst
echo

echo Translating  MemoryAccess/BasicTest/BasicTest.vm
python.exe vmtranslator.py no_bootstrap MemoryAccess/BasicTest/BasicTest.vm
CPUEmulator.bat MemoryAccess/BasicTest/BasicTest.tst
echo

echo Translating  MemoryAccess/PointerTest/PointerTest.vm
python.exe vmtranslator.py no_bootstrap MemoryAccess/PointerTest/PointerTest.vm
CPUEmulator.bat MemoryAccess/PointerTest/PointerTest.tst
echo

echo Translating  MemoryAccess/StaticTest/StaticTest.vm
python.exe vmtranslator.py no_bootstrap MemoryAccess/StaticTest/StaticTest.vm
CPUEmulator.bat MemoryAccess/StaticTest/StaticTest.tst
echo

echo Translating  MultiTest/
python.exe vmtranslator.py no_bootstrap MultiTest
CPUEmulator.bat MultiTest/MultiTest.tst
echo

echo Translating  SingleFileStaticTest/SingleFileStaticTest.vm
python.exe vmtranslator.py no_bootstrap SingleFileStaticTest/SingleFileStaticTest.vm
CPUEmulator.bat SingleFileStaticTest/SingleFileStaticTest.tst
echo

echo Translating  project7/StaticDirTest/
python.exe vmtranslator.py no_bootstrap StaticDirTest
CPUEmulator.bat StaticDirTest/StaticDirTest.tst
echo

echo Translating  project7/VMTest/
python.exe vmtranslator.py no_bootstrap VMTest
CPUEmulator.bat VMTest/VMTest.tst
echo