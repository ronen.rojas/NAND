class SymbolTable:
    """
    A symbol table that associates names with information needed for Jack
    compilation: type, kind, and running index.  The symbol table has
    2 nested scopes (class/subroutine).
    """
    CLASS_KIND = ('Static', 'Field')
    SUBROUTINE_KIND = ('Argument', 'Var')

    def __init__(self):
        self.class_scope = dict()
        self.subroutine_scope = dict()
        self.var_counter = {'Static': 0, 'Field': 0, 'Argument': 0, 'Var': 0}

    def start_subroutine(self):
        """
        Starts a new subroutine scope (i.e. erases all names in the previous
        subroutine's scope.)
        """
        self.subroutine_scope = dict()
        self.var_counter['Argument'] = 0
        self.var_counter['Var'] = 0

    def define(self, name, symbol_type, kind):
        """
        Defines a new identifier of a given name, type, and kind and assigns it
        a running index. STATIC and FIELD identifiers have a class scope, while
        ARG and VAR identifiers have a subroutine scope.
        :param name: name of the identifier (str).
        :param symbol_type: the type of the identifier (str)
        :param kind: the kind of the identifier (str)
        :return: No return value
        """
        if kind in self.CLASS_KIND:
            self.class_scope[name] = {'type': symbol_type, 'kind': kind,
                                      'index': self.var_count(kind)}
        if kind in self.SUBROUTINE_KIND:
            if name not in self.subroutine_scope:
                self.subroutine_scope[name] = {'type': symbol_type,
                                               'kind': kind,
                                               'index': self.var_count(kind)}
        self.var_counter[kind] += 1

    def var_count(self, kind):
        """
        Returns the number of variables of the given kind already defined in
        the current scope.
        :param kind: kind of the identifier
        :return: number of variables of the given kind (int)
        """
        return self.var_counter[kind]

    def kind_of(self, name):
        """
        Returns the kind of the named identifier in the current scope. Returns
        NONE if the identifier is unknown in the current scope.
        :param name: the name of the identifier (str).
        :return: kind of the named identifier (str).
        """
        return self.value_of(name, 'kind')

    def type_of(self, name):
        """
        Returns the type of the named identifier in the current scope.
        :param name: the name of the identifier (str)
        :return: type of the named identifier (str)
        """
        return self.value_of(name, 'type')

    def index_of(self, name):
        """
        Returns the index assigned to named identifier.
        :param name: the name of the identifier (str)
        :return: the index assigned to named identifier (int).
        """
        return self.value_of(name, 'index')

    def value_of(self, name, key):
        """
        Returns the value of the named identifier key.
        :param name: the name of the identifier (str).
        :param key: the key of the identifier.
        :return: the value of the identifier.
        """
        if name in self.subroutine_scope:
            return self.subroutine_scope[name][key]
        elif name in self.class_scope:
            return self.class_scope[name][key]





