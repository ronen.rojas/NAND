#!/bin/bash

# Seven
echo Compiling Seven
python3 compilationengine.py Seven/Main.jack
echo ------------------------------------------
echo
echo Comparing bit exact Seven/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Seven/Main.vm Seven/Main.vm_ref
echo ------------------------------------------
echo

# Average
echo Compiling Average
python3 compilationengine.py Average/Main.jack
echo ------------------------------------------
echo
echo Comparing bit exact Average/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Average/Main.vm Average/Main.vm_ref
echo ------------------------------------------
echo

# ComplexArrays
echo Compiling ComplexArrays
python3 compilationengine.py ComplexArrays/Main.jack
echo ------------------------------------------
echo
echo Comparing bit exact ComplexArrays/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh ComplexArrays/Main.vm ComplexArrays/Main.vm_ref
echo ------------------------------------------
echo

# ConvertToBin
echo Compiling ConvertToBin
python3 compilationengine.py ConvertToBin/Main.jack
echo ------------------------------------------
echo
echo Comparing bit exact ConvertToBin/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh ConvertToBin/Main.vm ConvertToBin/Main.vm_ref
echo ------------------------------------------
echo

# Square
echo Compiling Square
python3 compilationengine.py Square/
echo ------------------------------------------
echo
echo Comparing bit exact Square/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Square/Main.vm Square/Main.vm_ref
echo
echo Comparing bit exact Square/Square.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Square/Square.vm Square/Square.vm_ref
echo
echo Comparing bit exact Square/SquareGame.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Square/SquareGame.vm Square/SquareGame.vm_ref
echo ------------------------------------------
echo

# Pong
echo Compiling Pong
python3 compilationengine.py Pong/
echo ------------------------------------------
echo
echo Comparing bit exact Pong/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Pong/Main.vm Pong/Main.vm_ref
echo
echo Comparing bit exact Pong/Bat.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Pong/Bat.vm Pong/Bat.vm_ref
echo
echo Comparing bit exact Pong/Ball.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Pong/Ball.vm Pong/Ball.vm_ref
echo
echo Comparing bit exact Pong/PongGame.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh Pong/PongGame.vm Pong/PongGame.vm_ref
echo ------------------------------------------
echo

# StringTest
echo Compiling StringTest
python3 compilationengine.py StringTest/
echo ------------------------------------------
echo
echo Comparing bit exact StringTest/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh StringTest/Main.vm StringTest/Main.vm_ref
echo ------------------------------------------
echo

# 1
echo Compiling 1
python3 compilationengine.py 1/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory 1/
python3 tests.py 1/
echo ------------------------------------------

# 3
echo Compiling 3
python3 compilationengine.py 3/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory 3/
python3 tests.py 3/
echo ------------------------------------------

# 5
echo Compiling 5
python3 compilationengine.py 5/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory 5/
python3 tests.py 5/
echo ------------------------------------------

# ArrayTest
echo Compiling ArrayTest
python3 compilationengine.py ArrayTest/Main.jack
echo ------------------------------------------
echo
echo Comparing bit exact ArrayTest/Main.vm code
~/Downloads/nand2tetris/tools/TextComparer.sh ArrayTest/Main.vm ArrayTest/Main.vm_ref
echo ------------------------------------------
echo

# GASboing
echo Compiling GASboing
python3 compilationengine.py GASboing/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory GASboing/
python3 tests.py GASboing/
echo ------------------------------------------
echo

# GASchunky
echo Compiling GASchunky
python3 compilationengine.py GASchunky/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory GASchunky/
python3 tests.py GASchunky/
echo ------------------------------------------
echo

# GASscroller
echo Compiling GASscroller
python3 compilationengine.py GASscroller/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory GASscroller/
python3 tests.py GASscroller/
echo ------------------------------------------
echo

# JackScheme-master
echo Compiling JackScheme-master
python3 compilationengine.py JackScheme-master/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory JackScheme-master/
python3 tests.py JackScheme-master/
echo ------------------------------------------
echo

# sokoban
echo Compiling sokoban
python3 compilationengine.py sokoban/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory sokoban/
python3 tests.py sokoban/
echo ------------------------------------------
echo

# tictactoe-jack-master
echo Compiling tictactoe-jack-master
python3 compilationengine.py tictactoe-jack-master/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory tictactoe-jack-master/
python3 tests.py tictactoe-jack-master/
echo ------------------------------------------
echo

# galaxy
echo Compiling galaxy
python3 compilationengine.py galaxy/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory galaxy/
python3 tests.py galaxy/
echo ------------------------------------------
echo

# Snake
echo Compiling Snake
python3 compilationengine.py Snake/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory Snake/
python3 tests.py Snake/
echo ------------------------------------------
echo

# PigDice
echo Compiling PigDice
python3 compilationengine.py PigDice/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory PigDice/
python3 tests.py PigDice/
echo ------------------------------------------
echo

echo Compiling Hackenstein
python3 compilationengine.py Hackenstein/
echo ------------------------------------------
echo
echo Comparing bit exact vm code in directory Hackenstein/
python3 tests.py Hackenstein/
echo ------------------------------------------
echo
