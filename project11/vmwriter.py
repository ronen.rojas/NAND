class VMWriter:
    """
    This Class Emits VM command into a file, using the VM command syntax.
    """
    ARITHMETIC = '{0}\n'
    MEM_ACCESS = '{0} {1} {2}\n'
    FLOW = '{0} {1}\n'
    FUNCTION = MEM_ACCESS
    RETURN = 'return\n'
    COMMENT = '\n//{0}\n'

    def __init__(self, vm_file):
        """
        Creates a new file and prepares it for writing
        :param vm_file: input file (str)
        """
        self.vm_file = open(vm_file, 'w')

    def write_push(self, segment, index):
        """
        Writes a VM push command.
        :param segment: memory segment (str)
        :param index: index segment (int)
        :return: No return value
        """
        self.vm_file.write(self.MEM_ACCESS.format('push', segment, str(index)))

    def write_pop(self, segment, index):
        """
        Writes a VM pop command.
        :param segment: memory segment (str)
        :param index: index segment (int)
        :return: No return value
        """
        self.vm_file.write(self.MEM_ACCESS.format('pop', segment, str(index)))

    def write_arithmetic(self, command):
        """
        Writes a VM arithmetic command.
        :param command: arithmetic command (str)
        :return: No return value
        """
        self.vm_file.write(self.ARITHMETIC.format(command))

    def write_label(self, label):
        """
        Writes a VM label command.
        :param label: label (str)
        :return: No return value
        """
        self.vm_file.write(self.FLOW.format('label', label))

    def write_goto(self, label):
        """
        Writes a VM goto command.
        :param label: label (str)
        :return: No return value
        """
        self.vm_file.write(self.FLOW.format('goto', label))

    def write_if(self, label):
        """
        Writes a VM if-goto command.
        :param label: label (str)
        :return: No return value
        """
        self.vm_file.write(self.FLOW.format('if-goto', label))

    def write_call(self, name, n_args):
        """
        Writes a VM call command.
        :param name: name of the function (str)
        :param n_args: number of arguments (int)
        :return: No return value
        """
        self.vm_file.write(self.FUNCTION.format('call', name, n_args))

    def write_function(self, name, n_local):
        """
        Writes a VM function command.
        :param name: name of the function (str)
        :param n_local: number of locals (int)
        :return: No return value
        """
        self.vm_file.write(self.FUNCTION.format('function', name, n_local))

    def write_return(self):
        """
        Writes a VM return command.
        :return: No return value
        """
        self.vm_file.write(self.RETURN)

    def close(self):
        """
        Closes the output file
        :return: No return value
        """
        self.vm_file.close()

    def write_comment(self, comment):
        """
        Writes a comment in the vm file.
        :param comment: comment string (str)
        :return:
        """
        self.vm_file.write(self.COMMENT.format(comment))
