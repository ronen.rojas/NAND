import re


class JackTokenizer:
    """
    This class removes all comments and white space from the input stream and
    breaks it into Jack language tokens, as specified by the Jack grammar.
    """
    KEYWORDS = ['class', 'constructor', 'function', 'method', 'field',
                'static', 'var', 'int', 'char', 'boolean', 'void', 'true',
                'false', 'null', 'this', 'let', 'do', 'if', 'else', 'while',
                'return']
    SYMBOLS = ('{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/',
               '&', '|', '<', '>', '=', '~')
    SYMBOLS_RE = '[' + re.escape(''.join(SYMBOLS)) + ']'
    KEYWORD_RE = '(?!\w)|'.join(KEYWORDS) + '(?!\w)'
    INTEGER_RE = r'\d+'
    STRING_RE = r'"[^"\n]*"'
    IDENTIFIER_RE = r'[\w]+'
    WORD_RE = re.compile(KEYWORD_RE + '|' + SYMBOLS_RE + '|' + INTEGER_RE +
                         '|' + STRING_RE + '|' + IDENTIFIER_RE)
    # State machine mode for code cleaning
    CODE = 0
    CONSTANT_STR = 1
    COMMENT_LINE = 2
    COMMENT_BLOCK = 3

    def __init__(self, jack_file):
        """
        Opens the input file/stream and tokenize it.
        :param jack_file: The input jack file (str)
        """
        # Read the jack file raw text code
        self.jack_text = open(jack_file, 'r').read()
        # Clean all the comments
        self._clean_comments()
        self.token_lst = None
        self.tokenize()

    def __iter__(self):
        """
        Iterate for every token match.
        :return: Regular expressions matches in the Jack text.
        """
        return self.WORD_RE.finditer(self.jack_text)

    def _clean_comments(self):
        """
        Clean out comments in the jack code using state machine logic where
        code character that is not a code or a string constant gets taken off.
        :return: No return value
        """
        clean_str = ''
        idx = 0
        mode = self.CODE
        while idx < len(self.jack_text):
            if self.jack_text[idx] == '"':
                if mode == self.CODE:
                    mode = self.CONSTANT_STR
                elif mode == self.CONSTANT_STR:
                    mode = self.CODE
            if self.jack_text[idx] == '/':
                if mode == self.CODE:
                    if self.jack_text[idx + 1] == '/':
                        mode = self.COMMENT_LINE
                    elif self.jack_text[idx + 1] == '*':
                        mode = self.COMMENT_BLOCK
                if mode == self.COMMENT_BLOCK:
                    if self.jack_text[idx - 1] == '*':
                        mode = self.CODE
                        idx += 1
                        clean_str += ' '
                        continue
            if self.jack_text[idx] == '\n':
                if mode == self.COMMENT_LINE:
                    mode = self.CODE
            if mode == self.CODE or mode == self.CONSTANT_STR:
                clean_str += self.jack_text[idx]
            idx += 1
        self.jack_text = clean_str

    def tokenize(self):
        """
        Tokenize everything according to the jack grammar
        :return: No return value
        """
        self.token_lst = [self.tokenize_word(match.group()) for match in self]

    def tokenize_word(self, word):
        """
        Return the token for each work
        :param word: the word to be analyzed (str)
        :return: Token (str)
        """
        if re.match(self.KEYWORD_RE, word):
            return 'keyword', word
        elif re.match(self.SYMBOLS_RE, word):
            return 'symbol', word
        elif re.match(self.INTEGER_RE, word):
            return 'integerConstant', word
        elif re.match(self.STRING_RE, word):
            return 'stringConstant', word[1:-1]
        elif re.match(self.IDENTIFIER_RE, word):
            return 'identifier', word
