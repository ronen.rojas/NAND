#!/bin/bash
echo Runing test3.sh
./test3.sh
echo
FILE_LIST='a/Bit  a/PC  a/RAM64  a/RAM8  a/Register  b/RAM16K  b/RAM4K  b/RAM512'
for CHIP in $FILE_LIST; do
	echo Testing $CHIP.hdl
	HardwareSimulator.bat $CHIP.tst
	echo
done