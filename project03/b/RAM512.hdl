// This file is part of the materials accompanying the book 
// "The Elements of Computing Systems" by Nisan and Schocken, 
// MIT Press. Book site: www.idc.ac.il/tecs
// File name: projects/03/b/RAM512.hdl

/**
 * Memory of 512 registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load==1, then 
 * the in value is loaded into the memory location specified by address 
 * (the loaded value will be emitted to out from the next time step onward).
 */

CHIP RAM512 {
    IN in[16], load, address[9];
    OUT out[16];

    PARTS:
    // Put your code here:
	DMux8Way(in=load, sel=address[6..8], a=load1RAM64, b=load2RAM64, c=load3RAM64, d=load4RAM64, e=load5RAM64, f=load6RAM64, g=load7RAM64, h=load8RAM64);
	RAM64(in=in, load=load1RAM64, address=address[0..5], out=cell1size64);
	RAM64(in=in, load=load2RAM64, address=address[0..5], out=cell2size64);
	RAM64(in=in, load=load3RAM64, address=address[0..5], out=cell3size64);
	RAM64(in=in, load=load4RAM64, address=address[0..5], out=cell4size64);
	RAM64(in=in, load=load5RAM64, address=address[0..5], out=cell5size64);
	RAM64(in=in, load=load6RAM64, address=address[0..5], out=cell6size64);
	RAM64(in=in, load=load7RAM64, address=address[0..5], out=cell7size64);
	RAM64(in=in, load=load8RAM64, address=address[0..5], out=cell8size64);
    Mux8Way16(a=cell1size64, b=cell2size64, c=cell3size64, d=cell4size64, e=cell5size64, f=cell6size64, g=cell7size64, h=cell8size64, sel=address[6..8], out=out);
}