#!/bin/bash
cd project07

echo Basic Tests provided
echo -----------------------------------
echo Translating  StackArithmetic/SimpleAdd/SimpleAdd.vm
~/NAND/project07/VMtranslator StackArithmetic/SimpleAdd/SimpleAdd.vm
echo

echo Translating  StackArithmetic/StackTest/StackTest.vm
~/NAND/project07/VMtranslator ~/NAND/project07/StackArithmetic/StackTest/StackTest.vm
echo

echo Translating  MemoryAccess/BasicTest/BasicTest.vm
~/NAND/project07/VMtranslator /cs/usr/ronenr/NAND/project07/MemoryAccess/BasicTest/BasicTest.vm
echo

echo Translating  MemoryAccess/PointerTest/PointerTest.vm
~/NAND/project07/VMtranslator /cs/usr/ronenr/NAND/project07/MemoryAccess/PointerTest/PointerTest.vm
echo

echo Translating  MemoryAccess/StaticTest/StaticTest.vm
~/NAND/project07/VMtranslator /cs/usr/ronenr/NAND/project07/MemoryAccess/StaticTest/StaticTest.vm
echo

echo Translating  ProgramFlow/BasicLoop/BasicLoop.vm
~/NAND/project07/VMtranslator ~/NAND/project08/ProgramFlow/BasicLoop/BasicLoop.vm
echo

echo Translating  ProgramFlow/FibonacciSeries/FibonacciSeries.vm
~/NAND/project07/VMtranslator /cs/usr/ronenr/NAND/project08/ProgramFlow/FibonacciSeries/FibonacciSeries.vm
echo

echo Translating  FunctionCalls/SimpleFunction/SimpleFunction.vm
~/NAND/project07/VMtranslator ~/NAND/project08/FunctionCalls/SimpleFunction/SimpleFunction.vm
echo

echo Translating  FunctionCalls/NestedCall/
~/NAND/project07/VMtranslator /cs/usr/ronenr/NAND/project08/FunctionCalls/NestedCall/
echo

echo Bootstrap
echo -----------------------------------
echo Translating  FunctionCalls/FibonacciElement
~/NAND/project08/VMtranslator ~/NAND/project08/FunctionCalls/FibonacciElement
echo

echo Translating  FunctionCalls/StaticsTest
~/NAND/project08/VMtranslator ~/NAND/project08/FunctionCalls/StaticsTest
echo

echo Internet Tests no bootstrap
echo -----------------------------------
echo Translating  MultiTest/
~/NAND/project07/VMtranslator ~/NAND/project07/MultiTest/
echo

echo Translating  SingleFileStaticTest/SingleFileStaticTest.vm
~/NAND/project07/VMtranslator ~/NAND/project07/SingleFileStaticTest/SingleFileStaticTest.vm
echo

echo Translating  StaticDirTest
~/NAND/project07/VMtranslator StaticDirTest
echo

echo Translating  project7/VMTest/
~/NAND/project07/VMtranslator VMTest
echo

echo Translating  Tests/overflowTest/test.vm
~/NAND/project07/VMtranslator ~/NAND/project08/Tests/overflowTest/test.vm
echo

echo Internet Tests bootstrap
echo -----------------------------------
echo Translating  Tests/OverFlowTests/OverFlowTests
~/NAND/project08/VMtranslator /cs/usr/ronenr/NAND/project08/Tests/OverFlowTests/OverFlowTests
echo

echo Translating  Tests/test3/
~/NAND/project08/VMtranslator /cs/usr/ronenr/NAND/project08/Tests/test3
echo

echo Translating  Tests/test4/
~/NAND/project08/VMtranslator ~/NAND/project08/Tests/test4
echo

git diff -G '(^[^//.*\n])'

